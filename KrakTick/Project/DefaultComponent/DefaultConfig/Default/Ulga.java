/*********************************************************************
	Rhapsody	: 8.0.4
	Login		: Kronkiel
	Component	: DefaultComponent
	Configuration 	: DefaultConfig
	Model Element	: Ulga
//!	Generated Date	: Sun, 17, Nov 2013 
	File Path	: DefaultComponent/DefaultConfig/Default/Ulga.java
*********************************************************************/

package Default;


//----------------------------------------------------------------------------
// Default/Ulga.java                                                                  
//----------------------------------------------------------------------------

//## package Default 


//## class Ulga 
public enum Ulga {
    ULGOWY(2),
    NORMALNY(1),
    PRZETACZNIK(0);
    
    
    protected int value;		//## attribute value 
    
    
    // Constructors
    
    /**
     * @param dzielnik
    */
    //## operation Ulga(int) 
     Ulga(int dzielnik) {
        this.value = dzielnik;;
        //#[ operation Ulga(int) 
        //#]
    }
    //## auto_generated 
     Ulga() {
    }
    
    //## auto_generated 
    public int getValue() {
        return value;
    }
    
    //## auto_generated 
    public void setValue(int p_value) {
        value = p_value;
    }
    
}
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Default/Ulga.java
*********************************************************************/

