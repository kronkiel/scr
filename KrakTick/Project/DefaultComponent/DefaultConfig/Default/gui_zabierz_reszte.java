/*********************************************************************
	Rhapsody	: 8.0.4
	Login		: Michal
	Component	: DefaultComponent
	Configuration 	: DefaultConfig
	Model Element	: gui_zabierz_reszte
//!	Generated Date	: Thu, 21, Nov 2013 
	File Path	: DefaultComponent/DefaultConfig/Default/gui_zabierz_reszte.java
*********************************************************************/

package Default;

//## auto_generated
import com.ibm.rational.rhapsody.animation.*;
//## auto_generated
import com.ibm.rational.rhapsody.oxf.RiJEvent;
//## auto_generated
import com.ibm.rational.rhapsody.animcom.animMessages.*;

//----------------------------------------------------------------------------
// Default/gui_zabierz_reszte.java                                                                  
//----------------------------------------------------------------------------

//## package Default 


//## event gui_zabierz_reszte() 
public class gui_zabierz_reszte extends RiJEvent implements AnimatedEvent {
    
    public static final int gui_zabierz_reszte_Default_id = 18620;		//## ignore 
    
    
    // Constructors
    
    public  gui_zabierz_reszte() {
        lId = gui_zabierz_reszte_Default_id;
    }
    
    public boolean isTypeOf(long id) {
        return (gui_zabierz_reszte_Default_id==id);
    }
    
    //#[ ignore
    /** the animated event proxy */
    public static AnimEventClass animClass = new AnimEventClass("Default.gui_zabierz_reszte");
    /**  see com.ibm.rational.rhapsody.animation.AnimatedEvent interface */
    public Object getFieldValue(java.lang.reflect.Field f, Object userInstance) { 
         Object obj = null;
         try {
             obj = f.get(userInstance);
         } catch(Exception e) {
              java.lang.System.err.println("Exception: getting Field value: " + e);
              e.printStackTrace();
         }
         return obj;
    }
    /**  see com.ibm.rational.rhapsody.animation.AnimatedEvent interface */
    public void addAttributes(AnimAttributes msg) {      
    }
    public String toString() {
          String s="gui_zabierz_reszte(";      
          s += ")";
          return s;
    }
    //#]
    
}
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Default/gui_zabierz_reszte.java
*********************************************************************/

