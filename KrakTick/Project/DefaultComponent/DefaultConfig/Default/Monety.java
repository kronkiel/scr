/*********************************************************************
	Rhapsody	: 8.0.4
	Login		: Michal
	Component	: DefaultComponent
	Configuration 	: DefaultConfig
	Model Element	: Monety
//!	Generated Date	: Thu, 21, Nov 2013 
	File Path	: DefaultComponent/DefaultConfig/Default/Monety.java
*********************************************************************/

package Default;

//## auto_generated
import com.ibm.rational.rhapsody.animation.*;
//## auto_generated
import com.ibm.rational.rhapsody.animcom.animMessages.*;

//----------------------------------------------------------------------------
// Default/Monety.java                                                                  
//----------------------------------------------------------------------------

//## package Default 


//## class Monety 
public class Monety implements Animated {
    
    //#[ ignore
    // Instrumentation attributes (Animation)
    private Animate animate;
    
    public static AnimClass animClassMonety = new AnimClass("Default.Monety",false);
    //#]
    
    protected java.util.Map<Moneta, Integer> monety = new java.util.HashMap<Moneta, Integer>();		//## attribute monety 
    
    
    // Constructors
    
    //## operation Monety() 
    public  Monety() {
        try {
            animInstance().notifyConstructorEntered(animClassMonety.getUserClass(),
               new ArgData[] {
               });
        
        //#[ operation Monety() 
        uzupelnij(10);
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    /**
     * @param moneta
    */
    //## operation dodaj(Moneta) 
    public void dodaj(final Moneta moneta) {
        try {
            animInstance().notifyMethodEntered("dodaj",
               new ArgData[] {
                   new ArgData(Moneta.class, "moneta", AnimInstance.animToString(moneta))
               });
        
        //#[ operation dodaj(Moneta) 
        int teraz = monety.get(moneta);
        monety.put(moneta, teraz + 1);
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    /**
     * @param grosze
    */
    //## operation moznaWydac(int) 
    public boolean moznaWydac(int grosze) {
        try {
            animInstance().notifyMethodEntered("moznaWydac",
               new ArgData[] {
                   new ArgData(int.class, "grosze", AnimInstance.animToString(grosze))
               });
        
        //#[ operation moznaWydac(int) 
        int ilosc10gr = monety.get(Moneta.GR10);
        int ilosc20gr = monety.get(Moneta.GR20);
        int ilosc50gr = monety.get(Moneta.GR50);
        int ilosc1zl = monety.get(Moneta.ZL1);
        int ilosc2zl = monety.get(Moneta.ZL2);
        int ilosc5zl = monety.get(Moneta.ZL5);
                     
        while(grosze > 0){
        	if(grosze >= 500 && ilosc5zl > 0){
        	    grosze -= 500; 
        	    --ilosc5zl;
        	    continue; 
        	}
        	if(grosze >= 200 && ilosc2zl > 0){
        	    grosze -= 200; 
        	    --ilosc2zl;
        	    continue;
        	}
        	if(grosze >= 100 && ilosc1zl > 0){
        	    grosze -= 100;    
        	    --ilosc1zl;
        	    continue;
        	}
        	if(grosze >= 50 && ilosc50gr > 0){
        	    grosze -= 50;
        	    --ilosc50gr;
        	    continue; 
        	}
        	if(grosze >= 20 && ilosc20gr > 0){
        	    grosze -= 20;
        	    --ilosc20gr;
        	    continue;
        	}
        	if(grosze >= 10 && ilosc10gr > 0){
        	    grosze -= 10;   
        	    --ilosc10gr;
        	    continue;
        	}    
        	return false;
        }           
        return true;
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    /**
     * @param ile
    */
    //## operation uzupelnij(int) 
    public void uzupelnij(int ile) {
        try {
            animInstance().notifyMethodEntered("uzupelnij",
               new ArgData[] {
                   new ArgData(int.class, "ile", AnimInstance.animToString(ile))
               });
        
        //#[ operation uzupelnij(int) 
        monety.put(Moneta.GR10, ile);
        monety.put(Moneta.GR20, ile);
        monety.put(Moneta.GR50, ile);
        monety.put(Moneta.ZL1, ile);
        monety.put(Moneta.ZL2, ile);
        monety.put(Moneta.ZL5, ile);  
        
        
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    /**
     * @param ile
    */
    //## operation wydaj(int) 
    public java.util.List<Moneta> wydaj(int ile) {
        try {
            animInstance().notifyMethodEntered("wydaj",
               new ArgData[] {
                   new ArgData(int.class, "ile", AnimInstance.animToString(ile))
               });
        
        //#[ operation wydaj(int) 
        java.util.List<Moneta> reszta = new java.util.LinkedList<Moneta>();   
        
        int ilosc10gr = monety.get(Moneta.GR10);
        int ilosc20gr = monety.get(Moneta.GR20);
        int ilosc50gr = monety.get(Moneta.GR50);
        int ilosc1zl = monety.get(Moneta.ZL1);
        int ilosc2zl = monety.get(Moneta.ZL2);
        int ilosc5zl = monety.get(Moneta.ZL5);
                   
        while(ile > 0){
        	if(ile >= 500 && ilosc5zl > 0){
        	    ile -= 500; 
        	    --ilosc5zl;  
        	    reszta.add(Moneta.ZL5);
        	    continue; 
        	}
        	if(ile >= 200 && ilosc2zl > 0){
        	    ile -= 200; 
        	    --ilosc2zl;            
        	    reszta.add(Moneta.ZL2);
        	    continue;
        	}
        	if(ile >= 100 && ilosc1zl > 0){
        	    ile -= 100;    
        	    --ilosc1zl;          
        	    reszta.add(Moneta.ZL1);
        	    continue;
        	}
        	if(ile >= 50 && ilosc50gr > 0){
        	    ile -= 50;
        	    --ilosc50gr;         
        	    reszta.add(Moneta.GR50);
        	    continue; 
        	}
        	if(ile >= 20 && ilosc20gr > 0){
        	    ile -= 20;
        	    --ilosc20gr;            
        	    reszta.add(Moneta.GR20);
        	    continue;
        	}
        	if(ile >= 10 && ilosc10gr > 0){
        	    ile -= 10;   
        	    --ilosc10gr;         
        	    reszta.add(Moneta.GR10);
        	    continue;
        	}
        }      
             
        monety.put(Moneta.GR10, ilosc10gr);
        monety.put(Moneta.GR20, ilosc20gr);
        monety.put(Moneta.GR50, ilosc50gr);
        monety.put(Moneta.ZL1, ilosc1zl);
        monety.put(Moneta.ZL2, ilosc2zl);
        monety.put(Moneta.ZL5, ilosc5zl);   
        
        return reszta;
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    //## auto_generated 
    public java.util.Map<Moneta, Integer> getMonety() {
        return monety;
    }
    
    //## auto_generated 
    public void setMonety(java.util.Map<Moneta, Integer> p_monety) {
        monety = p_monety;
    }
    
    //#[ ignore
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public AnimClass getAnimClass() { 
        return animClassMonety; 
    }
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public Object getFieldValue(java.lang.reflect.Field f, Object userInstance) { 
         Object obj = null;
         try {
             obj = f.get(userInstance);
         } catch(Exception e) {
              java.lang.System.err.println("Exception: getting Field value: " + e);
              e.printStackTrace();
         }
         return obj;
    }
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public AnimInstance animInstance() {
        if (animate == null) 
            animate = new Animate(); 
        return animate; 
    } 
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public void addAttributes(AnimAttributes msg) {
        
        msg.add("monety", monety);
    }
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public void addRelations(AnimRelations msg) {
        
    }
    /** An inner class added as instrumentation for animation */
    public class Animate extends AnimInstance { 
        public  Animate() { 
            super(Monety.this); 
        } 
        public void addAttributes(AnimAttributes msg) {
            Monety.this.addAttributes(msg);
        }
        public void addRelations(AnimRelations msg) {
            Monety.this.addRelations(msg);
        }
        
    } 
    //#]
    
}
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Default/Monety.java
*********************************************************************/

