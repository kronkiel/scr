/*********************************************************************
	Rhapsody	: 8.0.4
	Login		: Michal
	Component	: DefaultComponent
	Configuration 	: DefaultConfig
	Model Element	: Moneta
//!	Generated Date	: Thu, 21, Nov 2013 
	File Path	: DefaultComponent/DefaultConfig/Default/Moneta.java
*********************************************************************/

package Default;


//----------------------------------------------------------------------------
// Default/Moneta.java                                                                  
//----------------------------------------------------------------------------

//## package Default 


//## class Moneta 
public enum Moneta {
    GR10(10),
    GR20(20),
    GR50(50),
    ZL1(100),
    ZL2(200),
    ZL5(500);
    
    
    protected int value;		//## attribute value 
    
    
    // Constructors
    
    /**
     * @param value
    */
    //## operation Moneta(int) 
     Moneta(int value) {
        //#[ operation Moneta(int) 
        this.value = value;
        //#]
    }
    //## auto_generated 
     Moneta() {
    }
    
    //## operation toString() 
    public String toString() {
        //#[ operation toString() 
        return String.format("%.2f", 1.0f * value / 100.0f);
        //#]
    }
    
    //## auto_generated 
    public int getValue() {
        return value;
    }
    
    //## auto_generated 
    public void setValue(int p_value) {
        value = p_value;
    }
    
}
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Default/Moneta.java
*********************************************************************/

