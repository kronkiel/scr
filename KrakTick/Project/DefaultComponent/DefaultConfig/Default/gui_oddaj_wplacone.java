/*********************************************************************
	Rhapsody	: 8.0.4
	Login		: Michal
	Component	: DefaultComponent
	Configuration 	: DefaultConfig
	Model Element	: gui_oddaj_wplacone
//!	Generated Date	: Thu, 21, Nov 2013 
	File Path	: DefaultComponent/DefaultConfig/Default/gui_oddaj_wplacone.java
*********************************************************************/

package Default;

//## auto_generated
import com.ibm.rational.rhapsody.animation.*;
//## auto_generated
import com.ibm.rational.rhapsody.oxf.RiJEvent;
//## auto_generated
import com.ibm.rational.rhapsody.animcom.animMessages.*;

//----------------------------------------------------------------------------
// Default/gui_oddaj_wplacone.java                                                                  
//----------------------------------------------------------------------------

//## package Default 


//## event gui_oddaj_wplacone() 
public class gui_oddaj_wplacone extends RiJEvent implements AnimatedEvent {
    
    public static final int gui_oddaj_wplacone_Default_id = 18616;		//## ignore 
    
    
    // Constructors
    
    public  gui_oddaj_wplacone() {
        lId = gui_oddaj_wplacone_Default_id;
    }
    
    public boolean isTypeOf(long id) {
        return (gui_oddaj_wplacone_Default_id==id);
    }
    
    //#[ ignore
    /** the animated event proxy */
    public static AnimEventClass animClass = new AnimEventClass("Default.gui_oddaj_wplacone");
    /**  see com.ibm.rational.rhapsody.animation.AnimatedEvent interface */
    public Object getFieldValue(java.lang.reflect.Field f, Object userInstance) { 
         Object obj = null;
         try {
             obj = f.get(userInstance);
         } catch(Exception e) {
              java.lang.System.err.println("Exception: getting Field value: " + e);
              e.printStackTrace();
         }
         return obj;
    }
    /**  see com.ibm.rational.rhapsody.animation.AnimatedEvent interface */
    public void addAttributes(AnimAttributes msg) {      
    }
    public String toString() {
          String s="gui_oddaj_wplacone(";      
          s += ")";
          return s;
    }
    //#]
    
}
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Default/gui_oddaj_wplacone.java
*********************************************************************/

