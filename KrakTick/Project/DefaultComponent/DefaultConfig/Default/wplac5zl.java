/*********************************************************************
	Rhapsody	: 8.0.4
	Login		: Kronkiel
	Component	: DefaultComponent
	Configuration 	: DefaultConfig
	Model Element	: wplac5zl
//!	Generated Date	: Sun, 17, Nov 2013 
	File Path	: DefaultComponent/DefaultConfig/Default/wplac5zl.java
*********************************************************************/

package Default;

//## auto_generated
import com.ibm.rational.rhapsody.animation.*;
//## auto_generated
import com.ibm.rational.rhapsody.oxf.RiJEvent;
//## auto_generated
import com.ibm.rational.rhapsody.animcom.animMessages.*;

//----------------------------------------------------------------------------
// Default/wplac5zl.java                                                                  
//----------------------------------------------------------------------------

//## package Default 


//## event wplac5zl() 
public class wplac5zl extends RiJEvent implements AnimatedEvent {
    
    public static final int wplac5zl_Default_id = 18627;		//## ignore 
    
    
    // Constructors
    
    public  wplac5zl() {
        lId = wplac5zl_Default_id;
    }
    
    public boolean isTypeOf(long id) {
        return (wplac5zl_Default_id==id);
    }
    
    //#[ ignore
    /** the animated event proxy */
    public static AnimEventClass animClass = new AnimEventClass("Default.wplac5zl");
    /**  see com.ibm.rational.rhapsody.animation.AnimatedEvent interface */
    public Object getFieldValue(java.lang.reflect.Field f, Object userInstance) { 
         Object obj = null;
         try {
             obj = f.get(userInstance);
         } catch(Exception e) {
              java.lang.System.err.println("Exception: getting Field value: " + e);
              e.printStackTrace();
         }
         return obj;
    }
    /**  see com.ibm.rational.rhapsody.animation.AnimatedEvent interface */
    public void addAttributes(AnimAttributes msg) {      
    }
    public String toString() {
          String s="wplac5zl(";      
          s += ")";
          return s;
    }
    //#]
    
}
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Default/wplac5zl.java
*********************************************************************/

