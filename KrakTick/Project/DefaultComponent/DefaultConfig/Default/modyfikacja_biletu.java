/*********************************************************************
	Rhapsody	: 8.0.4
	Login		: Kronkiel
	Component	: DefaultComponent
	Configuration 	: DefaultConfig
	Model Element	: modyfikacja_biletu
//!	Generated Date	: Sun, 17, Nov 2013 
	File Path	: DefaultComponent/DefaultConfig/Default/modyfikacja_biletu.java
*********************************************************************/

package Default;

//## auto_generated
import com.ibm.rational.rhapsody.animation.*;
//## auto_generated
import com.ibm.rational.rhapsody.oxf.RiJEvent;
//## auto_generated
import com.ibm.rational.rhapsody.animcom.animMessages.*;

//----------------------------------------------------------------------------
// Default/modyfikacja_biletu.java                                                                  
//----------------------------------------------------------------------------

//## package Default 


//## event modyfikacja_biletu() 
public class modyfikacja_biletu extends RiJEvent implements AnimatedEvent {
    
    public static final int modyfikacja_biletu_Default_id = 18617;		//## ignore 
    
    
    // Constructors
    
    public  modyfikacja_biletu() {
        lId = modyfikacja_biletu_Default_id;
    }
    
    public boolean isTypeOf(long id) {
        return (modyfikacja_biletu_Default_id==id);
    }
    
    //#[ ignore
    /** the animated event proxy */
    public static AnimEventClass animClass = new AnimEventClass("Default.modyfikacja_biletu");
    /**  see com.ibm.rational.rhapsody.animation.AnimatedEvent interface */
    public Object getFieldValue(java.lang.reflect.Field f, Object userInstance) { 
         Object obj = null;
         try {
             obj = f.get(userInstance);
         } catch(Exception e) {
              java.lang.System.err.println("Exception: getting Field value: " + e);
              e.printStackTrace();
         }
         return obj;
    }
    /**  see com.ibm.rational.rhapsody.animation.AnimatedEvent interface */
    public void addAttributes(AnimAttributes msg) {      
    }
    public String toString() {
          String s="modyfikacja_biletu(";      
          s += ")";
          return s;
    }
    //#]
    
}
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Default/modyfikacja_biletu.java
*********************************************************************/

