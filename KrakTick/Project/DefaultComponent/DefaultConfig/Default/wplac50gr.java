/*********************************************************************
	Rhapsody	: 8.0.4
	Login		: Kronkiel
	Component	: DefaultComponent
	Configuration 	: DefaultConfig
	Model Element	: wplac50gr
//!	Generated Date	: Sun, 17, Nov 2013 
	File Path	: DefaultComponent/DefaultConfig/Default/wplac50gr.java
*********************************************************************/

package Default;

//## auto_generated
import com.ibm.rational.rhapsody.animation.*;
//## auto_generated
import com.ibm.rational.rhapsody.oxf.RiJEvent;
//## auto_generated
import com.ibm.rational.rhapsody.animcom.animMessages.*;

//----------------------------------------------------------------------------
// Default/wplac50gr.java                                                                  
//----------------------------------------------------------------------------

//## package Default 


//## event wplac50gr() 
public class wplac50gr extends RiJEvent implements AnimatedEvent {
    
    public static final int wplac50gr_Default_id = 18624;		//## ignore 
    
    
    // Constructors
    
    public  wplac50gr() {
        lId = wplac50gr_Default_id;
    }
    
    public boolean isTypeOf(long id) {
        return (wplac50gr_Default_id==id);
    }
    
    //#[ ignore
    /** the animated event proxy */
    public static AnimEventClass animClass = new AnimEventClass("Default.wplac50gr");
    /**  see com.ibm.rational.rhapsody.animation.AnimatedEvent interface */
    public Object getFieldValue(java.lang.reflect.Field f, Object userInstance) { 
         Object obj = null;
         try {
             obj = f.get(userInstance);
         } catch(Exception e) {
              java.lang.System.err.println("Exception: getting Field value: " + e);
              e.printStackTrace();
         }
         return obj;
    }
    /**  see com.ibm.rational.rhapsody.animation.AnimatedEvent interface */
    public void addAttributes(AnimAttributes msg) {      
    }
    public String toString() {
          String s="wplac50gr(";      
          s += ")";
          return s;
    }
    //#]
    
}
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Default/wplac50gr.java
*********************************************************************/

