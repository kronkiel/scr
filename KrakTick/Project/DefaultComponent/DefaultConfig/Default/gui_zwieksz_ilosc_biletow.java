/*********************************************************************
	Rhapsody	: 8.0.4
	Login		: Michal
	Component	: DefaultComponent
	Configuration 	: DefaultConfig
	Model Element	: gui_zwieksz_ilosc_biletow
//!	Generated Date	: Thu, 21, Nov 2013 
	File Path	: DefaultComponent/DefaultConfig/Default/gui_zwieksz_ilosc_biletow.java
*********************************************************************/

package Default;

//## auto_generated
import com.ibm.rational.rhapsody.animation.*;
//## auto_generated
import com.ibm.rational.rhapsody.oxf.RiJEvent;
//## auto_generated
import com.ibm.rational.rhapsody.animcom.animMessages.*;

//----------------------------------------------------------------------------
// Default/gui_zwieksz_ilosc_biletow.java                                                                  
//----------------------------------------------------------------------------

//## package Default 


//## event gui_zwieksz_ilosc_biletow() 
public class gui_zwieksz_ilosc_biletow extends RiJEvent implements AnimatedEvent {
    
    public static final int gui_zwieksz_ilosc_biletow_Default_id = 18618;		//## ignore 
    
    
    // Constructors
    
    public  gui_zwieksz_ilosc_biletow() {
        lId = gui_zwieksz_ilosc_biletow_Default_id;
    }
    
    public boolean isTypeOf(long id) {
        return (gui_zwieksz_ilosc_biletow_Default_id==id);
    }
    
    //#[ ignore
    /** the animated event proxy */
    public static AnimEventClass animClass = new AnimEventClass("Default.gui_zwieksz_ilosc_biletow");
    /**  see com.ibm.rational.rhapsody.animation.AnimatedEvent interface */
    public Object getFieldValue(java.lang.reflect.Field f, Object userInstance) { 
         Object obj = null;
         try {
             obj = f.get(userInstance);
         } catch(Exception e) {
              java.lang.System.err.println("Exception: getting Field value: " + e);
              e.printStackTrace();
         }
         return obj;
    }
    /**  see com.ibm.rational.rhapsody.animation.AnimatedEvent interface */
    public void addAttributes(AnimAttributes msg) {      
    }
    public String toString() {
          String s="gui_zwieksz_ilosc_biletow(";      
          s += ")";
          return s;
    }
    //#]
    
}
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Default/gui_zwieksz_ilosc_biletow.java
*********************************************************************/

