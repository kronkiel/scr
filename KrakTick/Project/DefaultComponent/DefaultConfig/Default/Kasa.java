/*********************************************************************
	Rhapsody	: 8.0.4
	Login		: Michal
	Component	: DefaultComponent
	Configuration 	: DefaultConfig
	Model Element	: Kasa
//!	Generated Date	: Thu, 21, Nov 2013 
	File Path	: DefaultComponent/DefaultConfig/Default/Kasa.java
*********************************************************************/

package Default;

//## auto_generated
import com.ibm.rational.rhapsody.oxf.*;
//## auto_generated
import com.ibm.rational.rhapsody.animation.*;
//## auto_generated
import com.ibm.rational.rhapsody.oxf.states.*;
//## auto_generated
import com.ibm.rational.rhapsody.animcom.animMessages.*;

//----------------------------------------------------------------------------
// Default/Kasa.java                                                                  
//----------------------------------------------------------------------------

//## package Default 


//## class Kasa 
public class Kasa implements RiJStateConcept, Animated {
    
    //#[ ignore
    // Instrumentation attributes (Animation)
    private Animate animate;
    
    public static AnimClass animClassKasa = new AnimClass("Default.Kasa",false);
    //#]
    
    public Reactive reactive;		//## ignore 
    
    protected Monety monety = new Monety();		//## attribute monety 
    
    protected java.util.List<Moneta> wplacono = new java.util.LinkedList<Moneta>();		//## attribute wplacono 
    
    protected Automat automat;		//## link automat 
    
    //#[ ignore 
    public static final int RiJNonState=0;
    public static final int Wplac=1;
    public static final int Start=2;
    //#]
    protected int rootState_subState;		//## ignore 
    
    protected int rootState_active;		//## ignore 
    
    
    //## statechart_method 
    public RiJThread getThread() {
        return reactive.getThread();
    }
    
    //## statechart_method 
    public void schedTimeout(long delay, long tmID, RiJStateReactive reactive) {
        getThread().schedTimeout(delay, tmID, reactive);
    }
    
    //## statechart_method 
    public void unschedTimeout(long tmID, RiJStateReactive reactive) {
        getThread().unschedTimeout(tmID, reactive);
    }
    
    //## statechart_method 
    public boolean isIn(int state) {
        return reactive.isIn(state);
    }
    
    //## statechart_method 
    public boolean isCompleted(int state) {
        return reactive.isCompleted(state);
    }
    
    //## statechart_method 
    public RiJEventConsumer getEventConsumer() {
        return (RiJEventConsumer)reactive;
    }
    
    //## statechart_method 
    public void gen(RiJEvent event) {
        reactive._gen(event);
    }
    
    //## statechart_method 
    public void queueEvent(RiJEvent event) {
        reactive.queueEvent(event);
    }
    
    //## statechart_method 
    public int takeEvent(RiJEvent event) {
        return reactive.takeEvent(event);
    }
    
    // Constructors
    
    //## auto_generated 
    public  Kasa(RiJThread p_thread) {
        try {
            animInstance().notifyConstructorEntered(animClassKasa.getUserClass(),
               new ArgData[] {
               });
        
        reactive = new Reactive(p_thread);
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    //## operation ileWplacono() 
    protected int ileWplacono() {
        try {
            animInstance().notifyMethodEntered("ileWplacono",
               new ArgData[] {
               });
        
        //#[ operation ileWplacono() 
        int suma = 0;
        for(Moneta moneta : wplacono){
         	suma += moneta.getValue();
        }
        return suma;
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    /**
     * @param moneta
    */
    //## operation wplac(Moneta) 
    public void wplac(final Moneta moneta) {
        try {
            animInstance().notifyMethodEntered("wplac",
               new ArgData[] {
                   new ArgData(Moneta.class, "moneta", AnimInstance.animToString(moneta))
               });
        
        //#[ operation wplac(Moneta) 
        automat.ustaw_komunikat("Sprawdzanie monety...");
        try {
        	Thread.sleep(1500);
        } catch(Exception e) {}  
        
        wplacono.add(moneta);  
            
        if(automat.itsKoszyk.koszt() <= 0){
        	automat.oddaj_wplacone();
        }                          
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    //## operation wykonaj_zakup() 
    public boolean wykonaj_zakup() {
        try {
            animInstance().notifyMethodEntered("wykonaj_zakup",
               new ArgData[] {
               });
        
        //#[ operation wykonaj_zakup() 
        int ileWplacono = ileWplacono();
        int koszt = automat.itsKoszyk.koszt(); 
        int reszta = ileWplacono - koszt;
                                                                                                      
        if(!monety.moznaWydac(reszta)){
        	return false;
        }                                                                                            
                                                                                                                                                   
        //przyjmij wplacone monety
        for(int i = 0; i < wplacono.size(); ++i){
        	monety.dodaj(wplacono.get(i));
        }                   
        wplacono.clear();   
                                                                                           
        //wydaj reszte                          
        java.util.List<Moneta> wydaj = monety.wydaj(reszta);
        for(Moneta moneta : wydaj){              
        	automat.dodaj_reszte(moneta);
        }               
                                                                                           
        //wydrukuj bilety        
        for(Bilet bilet : automat.itsKoszyk.bilety){ 
        	try{
        		Thread.sleep(2000);
        	} catch (InterruptedException e){
        	 	e.printStackTrace();
        	}
         	automat.dodaj_bilet(bilet);
        }          
        automat.itsKoszyk.wyczysc();       
        
        return true;
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    //## auto_generated 
    public Monety getMonety() {
        return monety;
    }
    
    //## auto_generated 
    public void setMonety(Monety p_monety) {
        monety = p_monety;
    }
    
    //## auto_generated 
    public java.util.List<Moneta> getWplacono() {
        return wplacono;
    }
    
    //## auto_generated 
    public void setWplacono(java.util.List<Moneta> p_wplacono) {
        wplacono = p_wplacono;
    }
    
    //## auto_generated 
    public Automat getAutomat() {
        return automat;
    }
    
    //## auto_generated 
    public void __setAutomat(Automat p_Automat) {
        automat = p_Automat;
        if(p_Automat != null)
            {
                animInstance().notifyRelationAdded("automat", p_Automat);
            }
        else
            {
                animInstance().notifyRelationCleared("automat");
            }
    }
    
    //## auto_generated 
    public void _setAutomat(Automat p_Automat) {
        if(automat != null)
            {
                automat.__setItsKasa(null);
            }
        __setAutomat(p_Automat);
    }
    
    //## auto_generated 
    public void setAutomat(Automat p_Automat) {
        if(p_Automat != null)
            {
                p_Automat._setItsKasa(this);
            }
        _setAutomat(p_Automat);
    }
    
    //## auto_generated 
    public void _clearAutomat() {
        animInstance().notifyRelationCleared("automat");
        automat = null;
    }
    
    //## auto_generated 
    public boolean startBehavior() {
        boolean done = false;
        done = reactive.startBehavior();
        return done;
    }
    
    //## ignore 
    public class Reactive extends RiJStateReactive implements AnimatedReactive {
        
        // Default constructor 
        public Reactive() {
            this(RiJMainThread.instance());
        }
        
        
        // Constructors
        
        public  Reactive(RiJThread p_thread) {
            super(p_thread);
            initStatechart();
        }
        
        //## statechart_method 
        public boolean isIn(int state) {
            if(rootState_subState == state)
                {
                    return true;
                }
            return false;
        }
        
        //## statechart_method 
        public boolean isCompleted(int state) {
            return true;
        }
        
        //## statechart_method 
        public void rootState_add(AnimStates animStates) {
            animStates.add("ROOT");
            switch (rootState_subState) {
                case Start:
                {
                    Start_add(animStates);
                }
                break;
                case Wplac:
                {
                    Wplac_add(animStates);
                }
                break;
                default:
                    break;
            }
        }
        
        //## statechart_method 
        public void rootState_entDef() {
            {
                rootState_enter();
                rootStateEntDef();
            }
        }
        
        //## statechart_method 
        public int rootState_dispatchEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            switch (rootState_active) {
                case Start:
                {
                    res = Start_takeEvent(id);
                }
                break;
                case Wplac:
                {
                    res = Wplac_takeEvent(id);
                }
                break;
                default:
                    break;
            }
            return res;
        }
        
        //## statechart_method 
        public void Wplac_add(AnimStates animStates) {
            animStates.add("ROOT.Wplac");
        }
        
        //## statechart_method 
        public void Start_add(AnimStates animStates) {
            animStates.add("ROOT.Start");
        }
        
        //## auto_generated 
        protected void initStatechart() {
            rootState_subState = RiJNonState;
            rootState_active = RiJNonState;
        }
        
        //## statechart_method 
        public int WplacTakeNull() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            animInstance().notifyTransitionStarted("7");
            Wplac_exit();
            Start_entDef();
            animInstance().notifyTransitionEnded("7");
            res = RiJStateReactive.TAKE_EVENT_COMPLETE;
            return res;
        }
        
        //## statechart_method 
        public int Wplac_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.isTypeOf(RiJEvent.NULL_EVENT_ID))
                {
                    res = WplacTakeNull();
                }
            
            return res;
        }
        
        //## statechart_method 
        public int StartTakegui_wplac1zl() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            animInstance().notifyTransitionStarted("4");
            Start_exit();
            //#[ transition 4 
            wplac(Moneta.ZL1);
            //#]
            Wplac_entDef();
            animInstance().notifyTransitionEnded("4");
            res = RiJStateReactive.TAKE_EVENT_COMPLETE;
            return res;
        }
        
        //## statechart_method 
        public void Start_entDef() {
            Start_enter();
        }
        
        //## statechart_method 
        public int StartTakegui_wplac2zl() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            animInstance().notifyTransitionStarted("5");
            Start_exit();
            //#[ transition 5 
            wplac(Moneta.ZL2);
            //#]
            Wplac_entDef();
            animInstance().notifyTransitionEnded("5");
            res = RiJStateReactive.TAKE_EVENT_COMPLETE;
            return res;
        }
        
        //## statechart_method 
        public void Wplac_entDef() {
            Wplac_enter();
        }
        
        //## statechart_method 
        public void StartExit() {
        }
        
        //## statechart_method 
        public void Start_enter() {
            animInstance().notifyStateEntered("ROOT.Start");
            rootState_subState = Start;
            rootState_active = Start;
            StartEnter();
        }
        
        //## statechart_method 
        public int rootState_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            return res;
        }
        
        //## statechart_method 
        public int StartTakegui_wplac5zl() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            animInstance().notifyTransitionStarted("6");
            Start_exit();
            //#[ transition 6 
            wplac(Moneta.ZL5);
            //#]
            Wplac_entDef();
            animInstance().notifyTransitionEnded("6");
            res = RiJStateReactive.TAKE_EVENT_COMPLETE;
            return res;
        }
        
        //## statechart_method 
        public void StartEnter() {
            //#[ state Start.(Entry) 
            automat.odswiez();
            //#]
        }
        
        //## statechart_method 
        public void rootState_enter() {
            animInstance().notifyStateEntered("ROOT");
            rootStateEnter();
        }
        
        //## statechart_method 
        public void rootStateEnter() {
        }
        
        //## statechart_method 
        public void WplacExit() {
            //#[ state Wplac.(Exit) 
            automat.ustaw_komunikat(" ");
            //#]
        }
        
        //## statechart_method 
        public int StartTakegui_wplac50gr() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            animInstance().notifyTransitionStarted("3");
            Start_exit();
            //#[ transition 3 
            wplac(Moneta.GR50);
            //#]
            Wplac_entDef();
            animInstance().notifyTransitionEnded("3");
            res = RiJStateReactive.TAKE_EVENT_COMPLETE;
            return res;
        }
        
        //## statechart_method 
        public void Start_exit() {
            StartExit();
            animInstance().notifyStateExited("ROOT.Start");
        }
        
        //## statechart_method 
        public void WplacEnter() {
        }
        
        //## statechart_method 
        public void rootStateEntDef() {
            animInstance().notifyTransitionStarted("0");
            Start_entDef();
            animInstance().notifyTransitionEnded("0");
        }
        
        //## statechart_method 
        public int StartTakegui_wplac20gr() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            animInstance().notifyTransitionStarted("2");
            Start_exit();
            //#[ transition 2 
            wplac(Moneta.GR20);
            //#]
            Wplac_entDef();
            animInstance().notifyTransitionEnded("2");
            res = RiJStateReactive.TAKE_EVENT_COMPLETE;
            return res;
        }
        
        //## statechart_method 
        public int StartTakegui_wplac10gr() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            animInstance().notifyTransitionStarted("1");
            Start_exit();
            //#[ transition 1 
            wplac(Moneta.GR10);
            //#]
            Wplac_entDef();
            animInstance().notifyTransitionEnded("1");
            res = RiJStateReactive.TAKE_EVENT_COMPLETE;
            return res;
        }
        
        //## statechart_method 
        public void Wplac_exit() {
            popNullConfig();
            WplacExit();
            animInstance().notifyStateExited("ROOT.Wplac");
        }
        
        //## statechart_method 
        public void Wplac_enter() {
            animInstance().notifyStateEntered("ROOT.Wplac");
            pushNullConfig();
            rootState_subState = Wplac;
            rootState_active = Wplac;
            WplacEnter();
        }
        
        //## statechart_method 
        public void rootStateExit() {
        }
        
        //## statechart_method 
        public int Start_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.isTypeOf(gui_wplac50gr.gui_wplac50gr_Default_id))
                {
                    res = StartTakegui_wplac50gr();
                }
            else if(event.isTypeOf(gui_wplac20gr.gui_wplac20gr_Default_id))
                {
                    res = StartTakegui_wplac20gr();
                }
            else if(event.isTypeOf(gui_wplac1zl.gui_wplac1zl_Default_id))
                {
                    res = StartTakegui_wplac1zl();
                }
            else if(event.isTypeOf(gui_wplac10gr.gui_wplac10gr_Default_id))
                {
                    res = StartTakegui_wplac10gr();
                }
            else if(event.isTypeOf(gui_wplac2zl.gui_wplac2zl_Default_id))
                {
                    res = StartTakegui_wplac2zl();
                }
            else if(event.isTypeOf(gui_wplac5zl.gui_wplac5zl_Default_id))
                {
                    res = StartTakegui_wplac5zl();
                }
            
            return res;
        }
        
        /**  methods added just for design level debugging instrumentation */
        public boolean startBehavior() {
            try {
              animInstance().notifyBehavioralMethodEntered("startBehavior",
                  new ArgData[] {
                   });
              return super.startBehavior();
            }
            finally {
              animInstance().notifyMethodExit();
            }
        }
        public int takeEvent(RiJEvent event) { 
            try { 
              //animInstance().notifyTakeEvent(new AnimEvent(event));
              animInstance().notifyBehavioralMethodEntered("takeEvent",
                  new ArgData[] { new ArgData(RiJEvent.class, "event", event.toString())
                   });
              return super.takeEvent(event); 
            }
            finally { 
              animInstance().notifyMethodExit();
            }
        }
        /**  see com.ibm.rational.rhapsody.animation.AnimatedReactive interface */
        public AnimInstance animInstance() { 
            return Kasa.this.animInstance(); 
        }
        
    }
    //#[ ignore
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public AnimClass getAnimClass() { 
        return animClassKasa; 
    }
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public Object getFieldValue(java.lang.reflect.Field f, Object userInstance) { 
         Object obj = null;
         try {
             obj = f.get(userInstance);
         } catch(Exception e) {
              java.lang.System.err.println("Exception: getting Field value: " + e);
              e.printStackTrace();
         }
         return obj;
    }
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public AnimInstance animInstance() {
        if (animate == null) 
            animate = new Animate(); 
        return animate; 
    } 
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public void addAttributes(AnimAttributes msg) {
        
        msg.add("wplacono", wplacono);
        msg.add("monety", monety);
    }
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public void addRelations(AnimRelations msg) {
        
        msg.add("automat", false, true, automat);
    }
    /** An inner class added as instrumentation for animation */
    public class Animate extends AnimInstance { 
        public  Animate() { 
            super(Kasa.this); 
        } 
        public void addAttributes(AnimAttributes msg) {
            Kasa.this.addAttributes(msg);
        }
        public void addRelations(AnimRelations msg) {
            Kasa.this.addRelations(msg);
        }
        
        public void addStates(AnimStates msg) {
            if ((reactive != null) && (reactive.isTerminated() == false))
              reactive.rootState_add(msg);
        }
        
    } 
    //#]
    
}
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Default/Kasa.java
*********************************************************************/

