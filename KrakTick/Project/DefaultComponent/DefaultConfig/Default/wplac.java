/*********************************************************************
	Rhapsody	: 8.0.4
	Login		: Kronkiel
	Component	: DefaultComponent
	Configuration 	: DefaultConfig
	Model Element	: wplac
//!	Generated Date	: Sun, 17, Nov 2013 
	File Path	: DefaultComponent/DefaultConfig/Default/wplac.java
*********************************************************************/

package Default;

//## auto_generated
import com.ibm.rational.rhapsody.animation.*;
//## auto_generated
import com.ibm.rational.rhapsody.oxf.RiJEvent;
//## auto_generated
import com.ibm.rational.rhapsody.animcom.animMessages.*;

//----------------------------------------------------------------------------
// Default/wplac.java                                                                  
//----------------------------------------------------------------------------

//## package Default 


//## event wplac(Moneta) 
public class wplac extends RiJEvent implements AnimatedEvent {
    
    public static final int wplac_Default_id = 18622;		//## ignore 
    
    public Moneta moneta;
    
    // Constructors
    
    public  wplac() {
        lId = wplac_Default_id;
    }
    public  wplac(Moneta p_moneta) {
        lId = wplac_Default_id;
        moneta = p_moneta;
    }
    
    public boolean isTypeOf(long id) {
        return (wplac_Default_id==id);
    }
    
    //#[ ignore
    /** the animated event proxy */
    public static AnimEventClass animClass = new AnimEventClass("Default.wplac");
    /**  see com.ibm.rational.rhapsody.animation.AnimatedEvent interface */
    public Object getFieldValue(java.lang.reflect.Field f, Object userInstance) { 
         Object obj = null;
         try {
             obj = f.get(userInstance);
         } catch(Exception e) {
              java.lang.System.err.println("Exception: getting Field value: " + e);
              e.printStackTrace();
         }
         return obj;
    }
    /**  see com.ibm.rational.rhapsody.animation.AnimatedEvent interface */
    public void addAttributes(AnimAttributes msg) {      
          msg.add("moneta", moneta);
    }
    public String toString() {
          String s="wplac(";      
          s += "moneta=" + AnimInstance.animToString(moneta) + " ";
          s += ")";
          return s;
    }
    //#]
    
}
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Default/wplac.java
*********************************************************************/

