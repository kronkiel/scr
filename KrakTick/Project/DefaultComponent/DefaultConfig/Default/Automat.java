/*********************************************************************
	Rhapsody	: 8.0.4
	Login		: Michal
	Component	: DefaultComponent
	Configuration 	: DefaultConfig
	Model Element	: Automat
//!	Generated Date	: Thu, 21, Nov 2013 
	File Path	: DefaultComponent/DefaultConfig/Default/Automat.java
*********************************************************************/

package Default;

//## auto_generated
import com.ibm.rational.rhapsody.oxf.*;
//## auto_generated
import com.ibm.rational.rhapsody.animation.*;
//## auto_generated
import com.ibm.rational.rhapsody.oxf.states.*;
//## auto_generated
import com.ibm.rational.rhapsody.animcom.animMessages.*;

//----------------------------------------------------------------------------
// Default/Automat.java                                                                  
//----------------------------------------------------------------------------

//## package Default 


//## class Automat 
public class Automat implements RiJStateConcept, Animated {
    
    //#[ ignore
    // Instrumentation attributes (Animation)
    private Animate animate;
    
    public static AnimClass animClassAutomat = new AnimClass("Default.Automat",false);
    //#]
    
    public Reactive reactive;		//## ignore 
    
    protected String reszta = " ";		//## attribute reszta 
    
    protected String wydaneBilety = " ";		//## attribute wydaneBilety 
    
    protected String wyswietl = "0,00";		//## attribute wyswietl 
    
    protected AktualnaCena itsAktualnaCena;		//## classInstance itsAktualnaCena 
    
    protected Bileter itsBileter;		//## classInstance itsBileter 
    
    protected Kasa itsKasa;		//## classInstance itsKasa 
    
    protected Komunikat itsKomunikat;		//## classInstance itsKomunikat 
    
    protected Koszyk itsKoszyk;		//## classInstance itsKoszyk 
    
    //#[ ignore 
    public static final int RiJNonState=0;
    public static final int Uzupelnianie=1;
    public static final int Start=2;
    public static final int Dzialanie_uzytkownika=3;
    public static final int Anulowanie=4;
    //#]
    protected int rootState_subState;		//## ignore 
    
    protected int rootState_active;		//## ignore 
    
    public static final int Automat_Timeout_Uzupelnianie_id = 1;		//## ignore 
    
    
    //## statechart_method 
    public RiJThread getThread() {
        return reactive.getThread();
    }
    
    //## statechart_method 
    public void schedTimeout(long delay, long tmID, RiJStateReactive reactive) {
        getThread().schedTimeout(delay, tmID, reactive);
    }
    
    //## statechart_method 
    public void unschedTimeout(long tmID, RiJStateReactive reactive) {
        getThread().unschedTimeout(tmID, reactive);
    }
    
    //## statechart_method 
    public boolean isIn(int state) {
        return reactive.isIn(state);
    }
    
    //## statechart_method 
    public boolean isCompleted(int state) {
        return reactive.isCompleted(state);
    }
    
    //## statechart_method 
    public RiJEventConsumer getEventConsumer() {
        return (RiJEventConsumer)reactive;
    }
    
    //## statechart_method 
    public void gen(RiJEvent event) {
        reactive._gen(event);
    }
    
    //## statechart_method 
    public void queueEvent(RiJEvent event) {
        reactive.queueEvent(event);
    }
    
    //## statechart_method 
    public int takeEvent(RiJEvent event) {
        return reactive.takeEvent(event);
    }
    
    // Constructors
    
    //## auto_generated 
    public  Automat(RiJThread p_thread) {
        try {
            animInstance().notifyConstructorEntered(animClassAutomat.getUserClass(),
               new ArgData[] {
               });
        
        reactive = new Reactive(p_thread);
        initRelations(p_thread);
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    //## operation anuluj() 
    public void anuluj() {
        try {
            animInstance().notifyMethodEntered("anuluj",
               new ArgData[] {
               });
        
        //#[ operation anuluj() 
        oddaj_wplacone();
        itsKoszyk.wyczysc();
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    /**
     * @param bilet
    */
    //## operation dodaj_bilet(Bilet) 
    public void dodaj_bilet(final Bilet bilet) {
        try {
            animInstance().notifyMethodEntered("dodaj_bilet",
               new ArgData[] {
                   new ArgData(Bilet.class, "bilet", AnimInstance.animToString(bilet))
               });
        
        //#[ operation dodaj_bilet(Bilet) 
        setWydaneBilety(wydaneBilety + bilet.string() + "\n");
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    /**
     * @param moneta
    */
    //## operation dodaj_reszte(Moneta) 
    public void dodaj_reszte(final Moneta moneta) {
        try {
            animInstance().notifyMethodEntered("dodaj_reszte",
               new ArgData[] {
                   new ArgData(Moneta.class, "moneta", AnimInstance.animToString(moneta))
               });
        
        //#[ operation dodaj_reszte(Moneta) 
        try {
        	Thread.sleep(1000);
        } catch(Exception e) {}
        
        setReszta(reszta + moneta + " ");
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    //## operation oddaj_wplacone() 
    public void oddaj_wplacone() {
        try {
            animInstance().notifyMethodEntered("oddaj_wplacone",
               new ArgData[] {
               });
        
        //#[ operation oddaj_wplacone() 
        for(Moneta moneta : itsKasa.wplacono){
            dodaj_reszte(moneta);
        }                               
        itsKasa.wplacono.clear();
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    //## operation odswiez() 
    public void odswiez() {
        try {
            animInstance().notifyMethodEntered("odswiez",
               new ArgData[] {
               });
        
        //#[ operation odswiez() 
        
        
        int koszt = itsKoszyk.koszt();
        int wplacono = itsKasa.ileWplacono();
        int reszta = wplacono - koszt;
        
        if(reszta >= 0){
         	if(itsKasa.wykonaj_zakup()){
         		setWyswietl("0,00");          
         	}             
         	else{
         	 	ustaw_komunikat("Wrzuc odliczona kwote");       
         	 	oddaj_wplacone();
         	 	setWyswietl(String.format("%.2f", koszt / 100.0f));
         	}
        }
        else{
         	setWyswietl(String.format("%.2f", -reszta / 100.0f));
        }
                         
                         
        animInstance().notifyUpdatedAttr();
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    /**
     * @param komunikat
    */
    //## operation ustaw_komunikat(String) 
    public void ustaw_komunikat(final String komunikat) {
        try {
            animInstance().notifyMethodEntered("ustaw_komunikat",
               new ArgData[] {
                   new ArgData(String.class, "komunikat", AnimInstance.animToString(komunikat))
               });
        
        //#[ operation ustaw_komunikat(String) 
        itsKomunikat.ustaw_komunikat(komunikat);
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    //## auto_generated 
    public String getReszta() {
        return reszta;
    }
    
    //## auto_generated 
    public void setReszta(String p_reszta) {
        try {
        reszta = p_reszta;
        }
        finally {
            animInstance().notifyUpdatedAttr();
        }
    }
    
    //## auto_generated 
    public String getWydaneBilety() {
        return wydaneBilety;
    }
    
    //## auto_generated 
    public void setWydaneBilety(String p_wydaneBilety) {
        try {
        wydaneBilety = p_wydaneBilety;
        }
        finally {
            animInstance().notifyUpdatedAttr();
        }
    }
    
    //## auto_generated 
    public String getWyswietl() {
        return wyswietl;
    }
    
    //## auto_generated 
    public void setWyswietl(String p_wyswietl) {
        try {
        wyswietl = p_wyswietl;
        }
        finally {
            animInstance().notifyUpdatedAttr();
        }
    }
    
    //## auto_generated 
    public AktualnaCena getItsAktualnaCena() {
        return itsAktualnaCena;
    }
    
    //## auto_generated 
    public void __setItsAktualnaCena(AktualnaCena p_AktualnaCena) {
        itsAktualnaCena = p_AktualnaCena;
        if(p_AktualnaCena != null)
            {
                animInstance().notifyRelationAdded("itsAktualnaCena", p_AktualnaCena);
            }
        else
            {
                animInstance().notifyRelationCleared("itsAktualnaCena");
            }
    }
    
    //## auto_generated 
    public void _setItsAktualnaCena(AktualnaCena p_AktualnaCena) {
        if(itsAktualnaCena != null)
            {
                itsAktualnaCena.__setAutomat(null);
            }
        __setItsAktualnaCena(p_AktualnaCena);
    }
    
    //## auto_generated 
    public AktualnaCena newItsAktualnaCena(RiJThread p_thread) {
        itsAktualnaCena = new AktualnaCena(p_thread);
        itsAktualnaCena._setAutomat(this);
        animInstance().notifyRelationAdded("itsAktualnaCena", itsAktualnaCena);
        return itsAktualnaCena;
    }
    
    //## auto_generated 
    public void deleteItsAktualnaCena() {
        itsAktualnaCena.__setAutomat(null);
        animInstance().notifyRelationRemoved("itsAktualnaCena", itsAktualnaCena);
        itsAktualnaCena=null;
    }
    
    //## auto_generated 
    public Bileter getItsBileter() {
        return itsBileter;
    }
    
    //## auto_generated 
    public void __setItsBileter(Bileter p_Bileter) {
        itsBileter = p_Bileter;
        if(p_Bileter != null)
            {
                animInstance().notifyRelationAdded("itsBileter", p_Bileter);
            }
        else
            {
                animInstance().notifyRelationCleared("itsBileter");
            }
    }
    
    //## auto_generated 
    public void _setItsBileter(Bileter p_Bileter) {
        if(itsBileter != null)
            {
                itsBileter.__setAutomat(null);
            }
        __setItsBileter(p_Bileter);
    }
    
    //## auto_generated 
    public Bileter newItsBileter() {
        itsBileter = new Bileter();
        itsBileter._setAutomat(this);
        animInstance().notifyRelationAdded("itsBileter", itsBileter);
        return itsBileter;
    }
    
    //## auto_generated 
    public void deleteItsBileter() {
        itsBileter.__setAutomat(null);
        animInstance().notifyRelationRemoved("itsBileter", itsBileter);
        itsBileter=null;
    }
    
    //## auto_generated 
    public Kasa getItsKasa() {
        return itsKasa;
    }
    
    //## auto_generated 
    public void __setItsKasa(Kasa p_Kasa) {
        itsKasa = p_Kasa;
        if(p_Kasa != null)
            {
                animInstance().notifyRelationAdded("itsKasa", p_Kasa);
            }
        else
            {
                animInstance().notifyRelationCleared("itsKasa");
            }
    }
    
    //## auto_generated 
    public void _setItsKasa(Kasa p_Kasa) {
        if(itsKasa != null)
            {
                itsKasa.__setAutomat(null);
            }
        __setItsKasa(p_Kasa);
    }
    
    //## auto_generated 
    public Kasa newItsKasa(RiJThread p_thread) {
        itsKasa = new Kasa(p_thread);
        itsKasa._setAutomat(this);
        animInstance().notifyRelationAdded("itsKasa", itsKasa);
        return itsKasa;
    }
    
    //## auto_generated 
    public void deleteItsKasa() {
        itsKasa.__setAutomat(null);
        animInstance().notifyRelationRemoved("itsKasa", itsKasa);
        itsKasa=null;
    }
    
    //## auto_generated 
    public Komunikat getItsKomunikat() {
        return itsKomunikat;
    }
    
    //## auto_generated 
    public void __setItsKomunikat(Komunikat p_Komunikat) {
        itsKomunikat = p_Komunikat;
        if(p_Komunikat != null)
            {
                animInstance().notifyRelationAdded("itsKomunikat", p_Komunikat);
            }
        else
            {
                animInstance().notifyRelationCleared("itsKomunikat");
            }
    }
    
    //## auto_generated 
    public void _setItsKomunikat(Komunikat p_Komunikat) {
        if(itsKomunikat != null)
            {
                itsKomunikat.__setAutomat(null);
            }
        __setItsKomunikat(p_Komunikat);
    }
    
    //## auto_generated 
    public Komunikat newItsKomunikat(RiJThread p_thread) {
        itsKomunikat = new Komunikat(p_thread);
        itsKomunikat._setAutomat(this);
        animInstance().notifyRelationAdded("itsKomunikat", itsKomunikat);
        return itsKomunikat;
    }
    
    //## auto_generated 
    public void deleteItsKomunikat() {
        itsKomunikat.__setAutomat(null);
        animInstance().notifyRelationRemoved("itsKomunikat", itsKomunikat);
        itsKomunikat=null;
    }
    
    //## auto_generated 
    public Koszyk getItsKoszyk() {
        return itsKoszyk;
    }
    
    //## auto_generated 
    public void __setItsKoszyk(Koszyk p_Koszyk) {
        itsKoszyk = p_Koszyk;
        if(p_Koszyk != null)
            {
                animInstance().notifyRelationAdded("itsKoszyk", p_Koszyk);
            }
        else
            {
                animInstance().notifyRelationCleared("itsKoszyk");
            }
    }
    
    //## auto_generated 
    public void _setItsKoszyk(Koszyk p_Koszyk) {
        if(itsKoszyk != null)
            {
                itsKoszyk.__setAutomat(null);
            }
        __setItsKoszyk(p_Koszyk);
    }
    
    //## auto_generated 
    public Koszyk newItsKoszyk(RiJThread p_thread) {
        itsKoszyk = new Koszyk(p_thread);
        itsKoszyk._setAutomat(this);
        animInstance().notifyRelationAdded("itsKoszyk", itsKoszyk);
        return itsKoszyk;
    }
    
    //## auto_generated 
    public void deleteItsKoszyk() {
        itsKoszyk.__setAutomat(null);
        animInstance().notifyRelationRemoved("itsKoszyk", itsKoszyk);
        itsKoszyk=null;
    }
    
    //## auto_generated 
    protected void initRelations(RiJThread p_thread) {
        itsAktualnaCena = newItsAktualnaCena(p_thread);
        itsBileter = newItsBileter();
        itsKasa = newItsKasa(p_thread);
        itsKomunikat = newItsKomunikat(p_thread);
        itsKoszyk = newItsKoszyk(p_thread);
    }
    
    //## auto_generated 
    public boolean startBehavior() {
        boolean done = true;
        done &= itsAktualnaCena.startBehavior();
        done &= itsKasa.startBehavior();
        done &= itsKomunikat.startBehavior();
        done &= itsKoszyk.startBehavior();
        done &= reactive.startBehavior();
        return done;
    }
    
    //## ignore 
    public class Reactive extends RiJStateReactive implements AnimatedReactive {
        
        // Default constructor 
        public Reactive() {
            this(RiJMainThread.instance());
        }
        
        
        // Constructors
        
        public  Reactive(RiJThread p_thread) {
            super(p_thread);
            initStatechart();
        }
        
        //## statechart_method 
        public boolean isIn(int state) {
            if(rootState_subState == state)
                {
                    return true;
                }
            return false;
        }
        
        //## statechart_method 
        public boolean isCompleted(int state) {
            return true;
        }
        
        //## statechart_method 
        public void rootState_add(AnimStates animStates) {
            animStates.add("ROOT");
            switch (rootState_subState) {
                case Start:
                {
                    Start_add(animStates);
                }
                break;
                case Dzialanie_uzytkownika:
                {
                    Dzialanie_uzytkownika_add(animStates);
                }
                break;
                case Anulowanie:
                {
                    Anulowanie_add(animStates);
                }
                break;
                case Uzupelnianie:
                {
                    Uzupelnianie_add(animStates);
                }
                break;
                default:
                    break;
            }
        }
        
        //## statechart_method 
        public void rootState_entDef() {
            {
                rootState_enter();
                rootStateEntDef();
            }
        }
        
        //## statechart_method 
        public int rootState_dispatchEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            switch (rootState_active) {
                case Start:
                {
                    res = Start_takeEvent(id);
                }
                break;
                case Dzialanie_uzytkownika:
                {
                    res = Dzialanie_uzytkownika_takeEvent(id);
                }
                break;
                case Anulowanie:
                {
                    res = Anulowanie_takeEvent(id);
                }
                break;
                case Uzupelnianie:
                {
                    res = Uzupelnianie_takeEvent(id);
                }
                break;
                default:
                    break;
            }
            return res;
        }
        
        //## statechart_method 
        public void Uzupelnianie_add(AnimStates animStates) {
            animStates.add("ROOT.Uzupelnianie");
        }
        
        //## statechart_method 
        public void Start_add(AnimStates animStates) {
            animStates.add("ROOT.Start");
        }
        
        //## statechart_method 
        public void Dzialanie_uzytkownika_add(AnimStates animStates) {
            animStates.add("ROOT.Dzialanie_uzytkownika");
        }
        
        //## statechart_method 
        public void Anulowanie_add(AnimStates animStates) {
            animStates.add("ROOT.Anulowanie");
        }
        
        //## auto_generated 
        protected void initStatechart() {
            rootState_subState = RiJNonState;
            rootState_active = RiJNonState;
        }
        
        //## statechart_method 
        public int StartTakegui_uzupelnij_bilety() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            animInstance().notifyTransitionStarted("9");
            Start_exit();
            //#[ transition 9 
            itsBileter.uzupelnij(15);
            //#]
            Uzupelnianie_entDef();
            animInstance().notifyTransitionEnded("9");
            res = RiJStateReactive.TAKE_EVENT_COMPLETE;
            return res;
        }
        
        //## statechart_method 
        public void Dzialanie_uzytkownikaExit() {
        }
        
        //## statechart_method 
        public void Dzialanie_uzytkownikaEnter() {
            //#[ state Dzialanie_uzytkownika.(Entry) 
            ustaw_komunikat("Prosze odebrac wlasnosc");
            //#]
        }
        
        //## statechart_method 
        public void Start_entDef() {
            Start_enter();
        }
        
        //## statechart_method 
        public void Dzialanie_uzytkownika_exit() {
            popNullConfig();
            Dzialanie_uzytkownikaExit();
            animInstance().notifyStateExited("ROOT.Dzialanie_uzytkownika");
        }
        
        //## statechart_method 
        public int StartTakegui_oddaj_wplacone() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            animInstance().notifyTransitionStarted("1");
            Start_exit();
            Anulowanie_entDef();
            animInstance().notifyTransitionEnded("1");
            res = RiJStateReactive.TAKE_EVENT_COMPLETE;
            return res;
        }
        
        //## statechart_method 
        public void StartExit() {
        }
        
        //## statechart_method 
        public void Start_enter() {
            animInstance().notifyStateEntered("ROOT.Start");
            rootState_subState = Start;
            rootState_active = Start;
            StartEnter();
        }
        
        //## statechart_method 
        public int Uzupelnianie_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.isTypeOf(RiJEvent.TIMEOUT_EVENT_ID))
                {
                    res = UzupelnianieTakeRiJTimeout();
                }
            
            return res;
        }
        
        //## statechart_method 
        public int Anulowanie_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.isTypeOf(RiJEvent.NULL_EVENT_ID))
                {
                    res = AnulowanieTakeNull();
                }
            
            return res;
        }
        
        //## statechart_method 
        public int UzupelnianieTakeRiJTimeout() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.getTimeoutId() == Automat_Timeout_Uzupelnianie_id)
                {
                    animInstance().notifyTransitionStarted("8");
                    Uzupelnianie_exit();
                    //#[ transition 8 
                    ustaw_komunikat("Uzupelniono.");
                    //#]
                    Start_entDef();
                    animInstance().notifyTransitionEnded("8");
                    res = RiJStateReactive.TAKE_EVENT_COMPLETE;
                }
            return res;
        }
        
        //## statechart_method 
        public int rootState_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            return res;
        }
        
        //## statechart_method 
        public void Dzialanie_uzytkownika_enter() {
            animInstance().notifyStateEntered("ROOT.Dzialanie_uzytkownika");
            pushNullConfig();
            rootState_subState = Dzialanie_uzytkownika;
            rootState_active = Dzialanie_uzytkownika;
            Dzialanie_uzytkownikaEnter();
        }
        
        //## statechart_method 
        public int StartTakegui_anuluj() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            animInstance().notifyTransitionStarted("5");
            Start_exit();
            //#[ transition 5 
            itsKoszyk.wyczysc();
            //#]
            Anulowanie_entDef();
            animInstance().notifyTransitionEnded("5");
            res = RiJStateReactive.TAKE_EVENT_COMPLETE;
            return res;
        }
        
        //## statechart_method 
        public int StartTakegui_zabierz_bilety() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            animInstance().notifyTransitionStarted("3");
            Start_exit();
            //#[ transition 3 
            setWydaneBilety(" ");
            //#]
            Dzialanie_uzytkownika_entDef();
            animInstance().notifyTransitionEnded("3");
            res = RiJStateReactive.TAKE_EVENT_COMPLETE;
            return res;
        }
        
        //## statechart_method 
        public void StartEnter() {
            //#[ state Start.(Entry) 
            odswiez();
            //#]
        }
        
        //## statechart_method 
        public void Uzupelnianie_entDef() {
            Uzupelnianie_enter();
        }
        
        //## statechart_method 
        public void rootState_enter() {
            animInstance().notifyStateEntered("ROOT");
            rootStateEnter();
        }
        
        //## statechart_method 
        public void rootStateEnter() {
        }
        
        //## statechart_method 
        public void UzupelnianieExit() {
            itsRiJThread.unschedTimeout(Automat_Timeout_Uzupelnianie_id, this);
        }
        
        //## statechart_method 
        public int StartTakegui_uzupelnij_monety() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            animInstance().notifyTransitionStarted("7");
            Start_exit();
            //#[ transition 7 
            itsKasa.monety.uzupelnij(10);
            //#]
            Uzupelnianie_entDef();
            animInstance().notifyTransitionEnded("7");
            res = RiJStateReactive.TAKE_EVENT_COMPLETE;
            return res;
        }
        
        //## statechart_method 
        public void Uzupelnianie_enter() {
            animInstance().notifyStateEntered("ROOT.Uzupelnianie");
            rootState_subState = Uzupelnianie;
            rootState_active = Uzupelnianie;
            UzupelnianieEnter();
        }
        
        //## statechart_method 
        public void Anulowanie_enter() {
            animInstance().notifyStateEntered("ROOT.Anulowanie");
            pushNullConfig();
            rootState_subState = Anulowanie;
            rootState_active = Anulowanie;
            AnulowanieEnter();
        }
        
        //## statechart_method 
        public void AnulowanieEnter() {
            //#[ state Anulowanie.(Entry) 
            oddaj_wplacone();
            //#]
        }
        
        //## statechart_method 
        public void Anulowanie_entDef() {
            Anulowanie_enter();
        }
        
        //## statechart_method 
        public void Start_exit() {
            StartExit();
            animInstance().notifyStateExited("ROOT.Start");
        }
        
        //## statechart_method 
        public void UzupelnianieEnter() {
            //#[ state Uzupelnianie.(Entry) 
            ustaw_komunikat("Uzupelnianie zasobow...");
            //#]
            itsRiJThread.schedTimeout(2000, Automat_Timeout_Uzupelnianie_id, this, "ROOT.Uzupelnianie");
        }
        
        //## statechart_method 
        public void rootStateEntDef() {
            animInstance().notifyTransitionStarted("0");
            Start_entDef();
            animInstance().notifyTransitionEnded("0");
        }
        
        //## statechart_method 
        public void AnulowanieExit() {
        }
        
        //## statechart_method 
        public void Dzialanie_uzytkownika_entDef() {
            Dzialanie_uzytkownika_enter();
        }
        
        //## statechart_method 
        public int AnulowanieTakeNull() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            animInstance().notifyTransitionStarted("6");
            Anulowanie_exit();
            Start_entDef();
            animInstance().notifyTransitionEnded("6");
            res = RiJStateReactive.TAKE_EVENT_COMPLETE;
            return res;
        }
        
        //## statechart_method 
        public void Anulowanie_exit() {
            popNullConfig();
            AnulowanieExit();
            animInstance().notifyStateExited("ROOT.Anulowanie");
        }
        
        //## statechart_method 
        public int Dzialanie_uzytkownikaTakeNull() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            animInstance().notifyTransitionStarted("4");
            Dzialanie_uzytkownika_exit();
            Start_entDef();
            animInstance().notifyTransitionEnded("4");
            res = RiJStateReactive.TAKE_EVENT_COMPLETE;
            return res;
        }
        
        //## statechart_method 
        public void Uzupelnianie_exit() {
            UzupelnianieExit();
            animInstance().notifyStateExited("ROOT.Uzupelnianie");
        }
        
        //## statechart_method 
        public void rootStateExit() {
        }
        
        //## statechart_method 
        public int Start_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.isTypeOf(gui_uzupelnij_monety.gui_uzupelnij_monety_Default_id))
                {
                    res = StartTakegui_uzupelnij_monety();
                }
            else if(event.isTypeOf(gui_oddaj_wplacone.gui_oddaj_wplacone_Default_id))
                {
                    res = StartTakegui_oddaj_wplacone();
                }
            else if(event.isTypeOf(gui_zabierz_bilety.gui_zabierz_bilety_Default_id))
                {
                    res = StartTakegui_zabierz_bilety();
                }
            else if(event.isTypeOf(gui_uzupelnij_bilety.gui_uzupelnij_bilety_Default_id))
                {
                    res = StartTakegui_uzupelnij_bilety();
                }
            else if(event.isTypeOf(gui_anuluj.gui_anuluj_Default_id))
                {
                    res = StartTakegui_anuluj();
                }
            else if(event.isTypeOf(gui_zabierz_reszte.gui_zabierz_reszte_Default_id))
                {
                    res = StartTakegui_zabierz_reszte();
                }
            
            return res;
        }
        
        //## statechart_method 
        public int StartTakegui_zabierz_reszte() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            animInstance().notifyTransitionStarted("2");
            Start_exit();
            //#[ transition 2 
            setReszta(" ");
            //#]
            Dzialanie_uzytkownika_entDef();
            animInstance().notifyTransitionEnded("2");
            res = RiJStateReactive.TAKE_EVENT_COMPLETE;
            return res;
        }
        
        //## statechart_method 
        public int Dzialanie_uzytkownika_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.isTypeOf(RiJEvent.NULL_EVENT_ID))
                {
                    res = Dzialanie_uzytkownikaTakeNull();
                }
            
            return res;
        }
        
        /**  methods added just for design level debugging instrumentation */
        public boolean startBehavior() {
            try {
              animInstance().notifyBehavioralMethodEntered("startBehavior",
                  new ArgData[] {
                   });
              return super.startBehavior();
            }
            finally {
              animInstance().notifyMethodExit();
            }
        }
        public int takeEvent(RiJEvent event) { 
            try { 
              //animInstance().notifyTakeEvent(new AnimEvent(event));
              animInstance().notifyBehavioralMethodEntered("takeEvent",
                  new ArgData[] { new ArgData(RiJEvent.class, "event", event.toString())
                   });
              return super.takeEvent(event); 
            }
            finally { 
              animInstance().notifyMethodExit();
            }
        }
        /**  see com.ibm.rational.rhapsody.animation.AnimatedReactive interface */
        public AnimInstance animInstance() { 
            return Automat.this.animInstance(); 
        }
        
    }
    //#[ ignore
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public AnimClass getAnimClass() { 
        return animClassAutomat; 
    }
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public Object getFieldValue(java.lang.reflect.Field f, Object userInstance) { 
         Object obj = null;
         try {
             obj = f.get(userInstance);
         } catch(Exception e) {
              java.lang.System.err.println("Exception: getting Field value: " + e);
              e.printStackTrace();
         }
         return obj;
    }
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public AnimInstance animInstance() {
        if (animate == null) 
            animate = new Animate(); 
        return animate; 
    } 
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public void addAttributes(AnimAttributes msg) {
        
        msg.add("wyswietl", wyswietl);
        msg.add("reszta", reszta);
        msg.add("wydaneBilety", wydaneBilety);
    }
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public void addRelations(AnimRelations msg) {
        
        msg.add("itsKasa", true, true, itsKasa);
        msg.add("itsKoszyk", true, true, itsKoszyk);
        msg.add("itsBileter", true, true, itsBileter);
        msg.add("itsKomunikat", true, true, itsKomunikat);
        msg.add("itsAktualnaCena", true, true, itsAktualnaCena);
    }
    /** An inner class added as instrumentation for animation */
    public class Animate extends AnimInstance { 
        public  Animate() { 
            super(Automat.this); 
        } 
        public void addAttributes(AnimAttributes msg) {
            Automat.this.addAttributes(msg);
        }
        public void addRelations(AnimRelations msg) {
            Automat.this.addRelations(msg);
        }
        
        public void addStates(AnimStates msg) {
            if ((reactive != null) && (reactive.isTerminated() == false))
              reactive.rootState_add(msg);
        }
        
    } 
    //#]
    
}
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Default/Automat.java
*********************************************************************/

