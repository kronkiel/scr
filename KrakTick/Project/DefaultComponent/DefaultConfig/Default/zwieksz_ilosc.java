/*********************************************************************
	Rhapsody	: 8.0.4
	Login		: Michal
	Component	: DefaultComponent
	Configuration 	: DefaultConfig
	Model Element	: zwieksz_ilosc
//!	Generated Date	: Sun, 17, Nov 2013 
	File Path	: DefaultComponent/DefaultConfig/Default/zwieksz_ilosc.java
*********************************************************************/

package Default;

//## auto_generated
import com.ibm.rational.rhapsody.animation.*;
//## auto_generated
import com.ibm.rational.rhapsody.oxf.RiJEvent;
//## auto_generated
import com.ibm.rational.rhapsody.animcom.animMessages.*;

//----------------------------------------------------------------------------
// Default/zwieksz_ilosc.java                                                                  
//----------------------------------------------------------------------------

//## package Default 


//## event zwieksz_ilosc() 
public class zwieksz_ilosc extends RiJEvent implements AnimatedEvent {
    
    public static final int zwieksz_ilosc_Default_id = 18625;		//## ignore 
    
    
    // Constructors
    
    public  zwieksz_ilosc() {
        lId = zwieksz_ilosc_Default_id;
    }
    
    public boolean isTypeOf(long id) {
        return (zwieksz_ilosc_Default_id==id);
    }
    
    //#[ ignore
    /** the animated event proxy */
    public static AnimEventClass animClass = new AnimEventClass("Default.zwieksz_ilosc");
    /**  see com.ibm.rational.rhapsody.animation.AnimatedEvent interface */
    public Object getFieldValue(java.lang.reflect.Field f, Object userInstance) { 
         Object obj = null;
         try {
             obj = f.get(userInstance);
         } catch(Exception e) {
              java.lang.System.err.println("Exception: getting Field value: " + e);
              e.printStackTrace();
         }
         return obj;
    }
    /**  see com.ibm.rational.rhapsody.animation.AnimatedEvent interface */
    public void addAttributes(AnimAttributes msg) {      
    }
    public String toString() {
          String s="zwieksz_ilosc(";      
          s += ")";
          return s;
    }
    //#]
    
}
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Default/zwieksz_ilosc.java
*********************************************************************/

