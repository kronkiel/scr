/*********************************************************************
	Rhapsody	: 8.0.4
	Login		: Michal
	Component	: DefaultComponent
	Configuration 	: DefaultConfig
	Model Element	: AktualnaCena
//!	Generated Date	: Thu, 21, Nov 2013 
	File Path	: DefaultComponent/DefaultConfig/Default/AktualnaCena.java
*********************************************************************/

package Default;

//## auto_generated
import com.ibm.rational.rhapsody.oxf.*;
//## auto_generated
import com.ibm.rational.rhapsody.animation.*;
//## auto_generated
import com.ibm.rational.rhapsody.oxf.states.*;
//## auto_generated
import com.ibm.rational.rhapsody.animcom.animMessages.*;

//----------------------------------------------------------------------------
// Default/AktualnaCena.java                                                                  
//----------------------------------------------------------------------------

//## package Default 


//## class AktualnaCena 
public class AktualnaCena implements RiJActive, RiJStateConcept, Animated {
    
    //#[ ignore
    // Instrumentation attributes (Animation)
    private Animate animate;
    
    public static AnimClass animClassAktualnaCena = new AnimClass("Default.AktualnaCena",false);
    //#]
    
    protected RiJThread m_thread;		//## ignore 
    
    public Reactive reactive;		//## ignore 
    
    protected String wyswietlana_cena;		//## attribute wyswietlana_cena 
    
    protected Automat automat;		//## link automat 
    
    //#[ ignore 
    public static final int RiJNonState=0;
    public static final int wyswietlanie=1;
    //#]
    protected int rootState_subState;		//## ignore 
    
    protected int rootState_active;		//## ignore 
    
    public static final int AktualnaCena_Timeout_wyswietlanie_id = 1;		//## ignore 
    
    
    //## statechart_method 
    public RiJThread getThread() {
        return reactive.getThread();
    }
    
    //## statechart_method 
    public void schedTimeout(long delay, long tmID, RiJStateReactive reactive) {
        getThread().schedTimeout(delay, tmID, reactive);
    }
    
    //## statechart_method 
    public void unschedTimeout(long tmID, RiJStateReactive reactive) {
        getThread().unschedTimeout(tmID, reactive);
    }
    
    //## statechart_method 
    public boolean isIn(int state) {
        return reactive.isIn(state);
    }
    
    //## statechart_method 
    public boolean isCompleted(int state) {
        return reactive.isCompleted(state);
    }
    
    //## statechart_method 
    public RiJEventConsumer getEventConsumer() {
        return (RiJEventConsumer)reactive;
    }
    
    //## statechart_method 
    public void gen(RiJEvent event) {
        reactive._gen(event);
    }
    
    //## statechart_method 
    public void queueEvent(RiJEvent event) {
        reactive.queueEvent(event);
    }
    
    //## statechart_method 
    public void cancelEvent(RiJEvent event) {
        m_thread.cancelEvent(event);
    }
    
    //## statechart_method 
    public int takeEvent(RiJEvent event) {
        return reactive.takeEvent(event);
    }
    
    //## statechart_method 
    public void run() {
        m_thread.run();
    }
    
    //## statechart_method 
    public void start() {
        m_thread.start(this);
    }
    
    // Constructors
    
    //## auto_generated 
    public  AktualnaCena(RiJThread p_thread) {
        try {
            animInstance().notifyConstructorEntered(animClassAktualnaCena.getUserClass(),
               new ArgData[] {
               });
        
        m_thread = new RiJThread("AktualnaCena");
        reactive = new Reactive(m_thread);
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    //## operation odswiez() 
    public void odswiez() {
        try {
            animInstance().notifyMethodEntered("odswiez",
               new ArgData[] {
               });
        
        //#[ operation odswiez() 
        Koszyk k = automat.itsKoszyk;
        int suma = new Bilet(k.wybrany_czas, k.wybrany_rodzaj, k.wybrany_ulga).cena() * k.wybrany_ilosc;   
        
        int rabat = 0; 
        if(k.wybrany_ilosc >= 5) {
        	rabat = (5 * new Bilet(k.wybrany_czas, k.wybrany_rodzaj, k.wybrany_ulga).cena()) / 10;                
        }
        
        
        this.wyswietlana_cena = String.valueOf((suma-rabat) / 100.0f);
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    //## auto_generated 
    public String getWyswietlana_cena() {
        return wyswietlana_cena;
    }
    
    //## auto_generated 
    public void setWyswietlana_cena(String p_wyswietlana_cena) {
        try {
        wyswietlana_cena = p_wyswietlana_cena;
        }
        finally {
            animInstance().notifyUpdatedAttr();
        }
    }
    
    //## auto_generated 
    public Automat getAutomat() {
        return automat;
    }
    
    //## auto_generated 
    public void __setAutomat(Automat p_Automat) {
        automat = p_Automat;
        if(p_Automat != null)
            {
                animInstance().notifyRelationAdded("automat", p_Automat);
            }
        else
            {
                animInstance().notifyRelationCleared("automat");
            }
    }
    
    //## auto_generated 
    public void _setAutomat(Automat p_Automat) {
        if(automat != null)
            {
                automat.__setItsAktualnaCena(null);
            }
        __setAutomat(p_Automat);
    }
    
    //## auto_generated 
    public void setAutomat(Automat p_Automat) {
        if(p_Automat != null)
            {
                p_Automat._setItsAktualnaCena(this);
            }
        _setAutomat(p_Automat);
    }
    
    //## auto_generated 
    public void _clearAutomat() {
        animInstance().notifyRelationCleared("automat");
        automat = null;
    }
    
    //## auto_generated 
    public boolean startBehavior() {
        boolean done = false;
        done = reactive.startBehavior();
        if(done)
            {
                start();
            }
        return done;
    }
    
    //## ignore 
    public class Reactive extends RiJStateReactive implements AnimatedReactive {
        
        // Default constructor 
        public Reactive() {
            this(RiJMainThread.instance());
        }
        
        
        // Constructors
        
        public  Reactive(RiJThread p_thread) {
            super(p_thread);
            initStatechart();
        }
        
        //## statechart_method 
        public boolean isIn(int state) {
            if(rootState_subState == state)
                {
                    return true;
                }
            return false;
        }
        
        //## statechart_method 
        public boolean isCompleted(int state) {
            return true;
        }
        
        //## statechart_method 
        public void rootState_add(AnimStates animStates) {
            animStates.add("ROOT");
            if(rootState_subState == wyswietlanie)
                {
                    wyswietlanie_add(animStates);
                }
        }
        
        //## statechart_method 
        public void rootState_entDef() {
            {
                rootState_enter();
                rootStateEntDef();
            }
        }
        
        //## statechart_method 
        public int rootState_dispatchEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(rootState_active == wyswietlanie)
                {
                    res = wyswietlanie_takeEvent(id);
                }
            return res;
        }
        
        //## statechart_method 
        public void wyswietlanie_add(AnimStates animStates) {
            animStates.add("ROOT.wyswietlanie");
        }
        
        //## auto_generated 
        protected void initStatechart() {
            rootState_subState = RiJNonState;
            rootState_active = RiJNonState;
        }
        
        //## statechart_method 
        public void wyswietlanie_exit() {
            wyswietlanieExit();
            animInstance().notifyStateExited("ROOT.wyswietlanie");
        }
        
        //## statechart_method 
        public int wyswietlanieTakeRiJTimeout() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.getTimeoutId() == AktualnaCena_Timeout_wyswietlanie_id)
                {
                    animInstance().notifyTransitionStarted("1");
                    wyswietlanie_exit();
                    //#[ transition 1 
                    odswiez();
                    //#]
                    wyswietlanie_entDef();
                    animInstance().notifyTransitionEnded("1");
                    res = RiJStateReactive.TAKE_EVENT_COMPLETE;
                }
            return res;
        }
        
        //## statechart_method 
        public int rootState_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            return res;
        }
        
        //## statechart_method 
        public void rootState_enter() {
            animInstance().notifyStateEntered("ROOT");
            rootStateEnter();
        }
        
        //## statechart_method 
        public void rootStateEnter() {
        }
        
        //## statechart_method 
        public void wyswietlanie_enter() {
            animInstance().notifyStateEntered("ROOT.wyswietlanie");
            rootState_subState = wyswietlanie;
            rootState_active = wyswietlanie;
            wyswietlanieEnter();
        }
        
        //## statechart_method 
        public void rootStateEntDef() {
            animInstance().notifyTransitionStarted("0");
            wyswietlanie_entDef();
            animInstance().notifyTransitionEnded("0");
        }
        
        //## statechart_method 
        public void wyswietlanie_entDef() {
            wyswietlanie_enter();
        }
        
        //## statechart_method 
        public void wyswietlanieExit() {
            itsRiJThread.unschedTimeout(AktualnaCena_Timeout_wyswietlanie_id, this);
        }
        
        //## statechart_method 
        public void rootStateExit() {
        }
        
        //## statechart_method 
        public void wyswietlanieEnter() {
            itsRiJThread.schedTimeout(500, AktualnaCena_Timeout_wyswietlanie_id, this, "ROOT.wyswietlanie");
        }
        
        //## statechart_method 
        public int wyswietlanie_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.isTypeOf(RiJEvent.TIMEOUT_EVENT_ID))
                {
                    res = wyswietlanieTakeRiJTimeout();
                }
            
            return res;
        }
        
        /**  methods added just for design level debugging instrumentation */
        public boolean startBehavior() {
            try {
              animInstance().notifyBehavioralMethodEntered("startBehavior",
                  new ArgData[] {
                   });
              return super.startBehavior();
            }
            finally {
              animInstance().notifyMethodExit();
            }
        }
        public int takeEvent(RiJEvent event) { 
            try { 
              //animInstance().notifyTakeEvent(new AnimEvent(event));
              animInstance().notifyBehavioralMethodEntered("takeEvent",
                  new ArgData[] { new ArgData(RiJEvent.class, "event", event.toString())
                   });
              return super.takeEvent(event); 
            }
            finally { 
              animInstance().notifyMethodExit();
            }
        }
        /**  see com.ibm.rational.rhapsody.animation.AnimatedReactive interface */
        public AnimInstance animInstance() { 
            return AktualnaCena.this.animInstance(); 
        }
        
    }
    //#[ ignore
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public AnimClass getAnimClass() { 
        return animClassAktualnaCena; 
    }
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public Object getFieldValue(java.lang.reflect.Field f, Object userInstance) { 
         Object obj = null;
         try {
             obj = f.get(userInstance);
         } catch(Exception e) {
              java.lang.System.err.println("Exception: getting Field value: " + e);
              e.printStackTrace();
         }
         return obj;
    }
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public AnimInstance animInstance() {
        if (animate == null) 
            animate = new Animate(); 
        return animate; 
    } 
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public void addAttributes(AnimAttributes msg) {
        
        msg.add("wyswietlana_cena", wyswietlana_cena);
    }
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public void addRelations(AnimRelations msg) {
        
        msg.add("automat", false, true, automat);
    }
    /** An inner class added as instrumentation for animation */
    public class Animate extends AnimInstance { 
        public  Animate() { 
            super(AktualnaCena.this); 
        } 
        public void addAttributes(AnimAttributes msg) {
            AktualnaCena.this.addAttributes(msg);
        }
        public void addRelations(AnimRelations msg) {
            AktualnaCena.this.addRelations(msg);
        }
        
        public void addStates(AnimStates msg) {
            if ((reactive != null) && (reactive.isTerminated() == false))
              reactive.rootState_add(msg);
        }
        
    } 
    //#]
    
}
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Default/AktualnaCena.java
*********************************************************************/

