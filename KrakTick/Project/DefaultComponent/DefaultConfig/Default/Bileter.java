/*********************************************************************
	Rhapsody	: 8.0.4
	Login		: Michal
	Component	: DefaultComponent
	Configuration 	: DefaultConfig
	Model Element	: Bileter
//!	Generated Date	: Thu, 21, Nov 2013 
	File Path	: DefaultComponent/DefaultConfig/Default/Bileter.java
*********************************************************************/

package Default;

//## auto_generated
import com.ibm.rational.rhapsody.animation.*;
//## auto_generated
import com.ibm.rational.rhapsody.animcom.animMessages.*;

//----------------------------------------------------------------------------
// Default/Bileter.java                                                                  
//----------------------------------------------------------------------------

//## package Default 


//## class Bileter 
public class Bileter implements Animated {
    
    //#[ ignore
    // Instrumentation attributes (Animation)
    private Animate animate;
    
    public static AnimClass animClassBileter = new AnimClass("Default.Bileter",false);
    //#]
    
    protected java.util.Map<Bilet.Czas, java.util.Map<Bilet.Rodzaj, java.util.Map<Bilet.Ulga, Integer>>> biletyCRUImap = new java.util.HashMap<Bilet.Czas, java.util.Map<Bilet.Rodzaj, java.util.Map<Bilet.Ulga, Integer>>>();		//## attribute biletyCRUImap 
    
    protected Automat automat;		//## link automat 
    
    
    // Constructors
    
    //## operation Bileter() 
    public  Bileter() {
        java.util.HashMap<Bilet.Ulga, Integer> UImap = new java.util.HashMap<Bilet.Ulga, Integer>();   
        UImap.put(Bilet.Ulga.NORMALNY, 0);
        UImap.put(Bilet.Ulga.ULGOWY, 0);
        
        java.util.HashMap<Bilet.Rodzaj, java.util.Map<Bilet.Ulga, Integer>> RUImap = new java.util.HashMap<Bilet.Rodzaj, java.util.Map<Bilet.Ulga, Integer>>();
        RUImap.put(Bilet.Rodzaj.AGLOMERACYJNY, new java.util.HashMap(UImap));
        RUImap.put(Bilet.Rodzaj.MIEJSKI, new java.util.HashMap(UImap));      
        
        java.util.HashMap<Bilet.Czas, java.util.Map<Bilet.Rodzaj, java.util.Map<Bilet.Ulga, Integer>>> CRUImap = new java.util.HashMap<Bilet.Czas, java.util.Map<Bilet.Rodzaj, java.util.Map<Bilet.Ulga, Integer>>>();
        CRUImap.put(Bilet.Czas.MIN_20, new java.util.HashMap(RUImap));
        CRUImap.put(Bilet.Czas.MIN_40, new java.util.HashMap(RUImap));
        CRUImap.put(Bilet.Czas.MIN_60, new java.util.HashMap(RUImap));
                         
        this.biletyCRUImap = CRUImap;          
        uzupelnij(5);;
        try {
            animInstance().notifyConstructorEntered(animClassBileter.getUserClass(),
               new ArgData[] {
               });
        
        //#[ operation Bileter() 
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    /**
     * @param bilet
    */
    //## operation ileJest(Bilet) 
    public int ileJest(final Bilet bilet) {
        try {
            animInstance().notifyMethodEntered("ileJest",
               new ArgData[] {
                   new ArgData(Bilet.class, "bilet", AnimInstance.animToString(bilet))
               });
        
        //#[ operation ileJest(Bilet) 
        return biletyCRUImap.get(bilet.czas).get(bilet.rodzaj).get(bilet.ulga);
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    /**
     * @param ile
    */
    //## operation uzupelnij(int) 
    public void uzupelnij(int ile) {
        try {
            animInstance().notifyMethodEntered("uzupelnij",
               new ArgData[] {
                   new ArgData(int.class, "ile", AnimInstance.animToString(ile))
               });
        
        //#[ operation uzupelnij(int) 
        java.util.Map RUI20 = biletyCRUImap.get(Bilet.Czas.MIN_20);
        java.util.Map RUI40 = biletyCRUImap.get(Bilet.Czas.MIN_40);
        java.util.Map RUI60 = biletyCRUImap.get(Bilet.Czas.MIN_60);
        
        java.util.Map M20 = (java.util.Map) RUI20.get(Bilet.Rodzaj.MIEJSKI);
        java.util.Map M40 = (java.util.Map) RUI40.get(Bilet.Rodzaj.MIEJSKI);
        java.util.Map M60 = (java.util.Map) RUI60.get(Bilet.Rodzaj.MIEJSKI);
        java.util.Map A20 = (java.util.Map) RUI20.get(Bilet.Rodzaj.AGLOMERACYJNY);
        java.util.Map A40 = (java.util.Map) RUI40.get(Bilet.Rodzaj.AGLOMERACYJNY);
        java.util.Map A60 = (java.util.Map) RUI60.get(Bilet.Rodzaj.AGLOMERACYJNY); 
        
        M20.put(Bilet.Ulga.NORMALNY, ile);
        M40.put(Bilet.Ulga.NORMALNY, ile);
        M60.put(Bilet.Ulga.NORMALNY, ile);
        A20.put(Bilet.Ulga.NORMALNY, ile);
        A40.put(Bilet.Ulga.NORMALNY, ile);
        A60.put(Bilet.Ulga.NORMALNY, ile);                  
        
        M20.put(Bilet.Ulga.ULGOWY, ile);
        M40.put(Bilet.Ulga.ULGOWY, ile);
        M60.put(Bilet.Ulga.ULGOWY, ile);
        A20.put(Bilet.Ulga.ULGOWY, ile);
        A40.put(Bilet.Ulga.ULGOWY, ile);
        A60.put(Bilet.Ulga.ULGOWY, ile);
               
               
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    /**
     * @param bilet
    */
    //## operation zabierz(Bilet) 
    public void zabierz(final Bilet bilet) {
        try {
            animInstance().notifyMethodEntered("zabierz",
               new ArgData[] {
                   new ArgData(Bilet.class, "bilet", AnimInstance.animToString(bilet))
               });
        
        //#[ operation zabierz(Bilet) 
        int val = this.biletyCRUImap.get(bilet.czas).get(bilet.rodzaj).get(bilet.ulga);
        --val;
        this.biletyCRUImap.get(bilet.czas).get(bilet.rodzaj).put(bilet.ulga, val);
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    /**
     * @param bilet
    */
    //## operation zwroc(Bilet) 
    public void zwroc(final Bilet bilet) {
        try {
            animInstance().notifyMethodEntered("zwroc",
               new ArgData[] {
                   new ArgData(Bilet.class, "bilet", AnimInstance.animToString(bilet))
               });
        
        //#[ operation zwroc(Bilet) 
        int ile = biletyCRUImap.get(bilet.czas).get(bilet.rodzaj).get(bilet.ulga);
        ++ile;
         biletyCRUImap.get(bilet.czas).get(bilet.rodzaj).put(bilet.ulga, ile);
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    //## auto_generated 
    public java.util.Map<Bilet.Czas, java.util.Map<Bilet.Rodzaj, java.util.Map<Bilet.Ulga, Integer>>> getBiletyCRUImap() {
        return biletyCRUImap;
    }
    
    //## auto_generated 
    public void setBiletyCRUImap(java.util.Map<Bilet.Czas, java.util.Map<Bilet.Rodzaj, java.util.Map<Bilet.Ulga, Integer>>> p_biletyCRUImap) {
        biletyCRUImap = p_biletyCRUImap;
    }
    
    //## auto_generated 
    public Automat getAutomat() {
        return automat;
    }
    
    //## auto_generated 
    public void __setAutomat(Automat p_Automat) {
        automat = p_Automat;
        if(p_Automat != null)
            {
                animInstance().notifyRelationAdded("automat", p_Automat);
            }
        else
            {
                animInstance().notifyRelationCleared("automat");
            }
    }
    
    //## auto_generated 
    public void _setAutomat(Automat p_Automat) {
        if(automat != null)
            {
                automat.__setItsBileter(null);
            }
        __setAutomat(p_Automat);
    }
    
    //## auto_generated 
    public void setAutomat(Automat p_Automat) {
        if(p_Automat != null)
            {
                p_Automat._setItsBileter(this);
            }
        _setAutomat(p_Automat);
    }
    
    //## auto_generated 
    public void _clearAutomat() {
        animInstance().notifyRelationCleared("automat");
        automat = null;
    }
    
    //#[ ignore
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public AnimClass getAnimClass() { 
        return animClassBileter; 
    }
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public Object getFieldValue(java.lang.reflect.Field f, Object userInstance) { 
         Object obj = null;
         try {
             obj = f.get(userInstance);
         } catch(Exception e) {
              java.lang.System.err.println("Exception: getting Field value: " + e);
              e.printStackTrace();
         }
         return obj;
    }
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public AnimInstance animInstance() {
        if (animate == null) 
            animate = new Animate(); 
        return animate; 
    } 
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public void addAttributes(AnimAttributes msg) {
        
        msg.add("biletyCRUImap", biletyCRUImap);
    }
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public void addRelations(AnimRelations msg) {
        
        msg.add("automat", false, true, automat);
    }
    /** An inner class added as instrumentation for animation */
    public class Animate extends AnimInstance { 
        public  Animate() { 
            super(Bileter.this); 
        } 
        public void addAttributes(AnimAttributes msg) {
            Bileter.this.addAttributes(msg);
        }
        public void addRelations(AnimRelations msg) {
            Bileter.this.addRelations(msg);
        }
        
    } 
    //#]
    
}
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Default/Bileter.java
*********************************************************************/

