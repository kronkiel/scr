/*********************************************************************
	Rhapsody	: 8.0.4
	Login		: Michal
	Component	: DefaultComponent
	Configuration 	: DefaultConfig
	Model Element	: Komunikat
//!	Generated Date	: Thu, 21, Nov 2013 
	File Path	: DefaultComponent/DefaultConfig/Default/Komunikat.java
*********************************************************************/

package Default;

//## auto_generated
import com.ibm.rational.rhapsody.oxf.*;
//## auto_generated
import com.ibm.rational.rhapsody.animation.*;
//## auto_generated
import com.ibm.rational.rhapsody.oxf.states.*;
//## auto_generated
import com.ibm.rational.rhapsody.animcom.animMessages.*;

//----------------------------------------------------------------------------
// Default/Komunikat.java                                                                  
//----------------------------------------------------------------------------

//## package Default 


//## class Komunikat 
public class Komunikat implements RiJActive, RiJStateConcept, Animated {
    
    //#[ ignore
    // Instrumentation attributes (Animation)
    private Animate animate;
    
    public static AnimClass animClassKomunikat = new AnimClass("Default.Komunikat",false);
    //#]
    
    protected RiJThread m_thread;		//## ignore 
    
    public Reactive reactive;		//## ignore 
    
    protected String komunikat = " ";		//## attribute komunikat 
    
    protected Automat automat;		//## link automat 
    
    //#[ ignore 
    public static final int RiJNonState=0;
    public static final int wyswietlanie=1;
    public static final int pusty=2;
    //#]
    protected int rootState_subState;		//## ignore 
    
    protected int rootState_active;		//## ignore 
    
    public static final int Komunikat_Timeout_wyswietlanie_id = 1;		//## ignore 
    
    
    //## statechart_method 
    public RiJThread getThread() {
        return reactive.getThread();
    }
    
    //## statechart_method 
    public void schedTimeout(long delay, long tmID, RiJStateReactive reactive) {
        getThread().schedTimeout(delay, tmID, reactive);
    }
    
    //## statechart_method 
    public void unschedTimeout(long tmID, RiJStateReactive reactive) {
        getThread().unschedTimeout(tmID, reactive);
    }
    
    //## statechart_method 
    public boolean isIn(int state) {
        return reactive.isIn(state);
    }
    
    //## statechart_method 
    public boolean isCompleted(int state) {
        return reactive.isCompleted(state);
    }
    
    //## statechart_method 
    public RiJEventConsumer getEventConsumer() {
        return (RiJEventConsumer)reactive;
    }
    
    //## statechart_method 
    public void gen(RiJEvent event) {
        reactive._gen(event);
    }
    
    //## statechart_method 
    public void queueEvent(RiJEvent event) {
        reactive.queueEvent(event);
    }
    
    //## statechart_method 
    public void cancelEvent(RiJEvent event) {
        m_thread.cancelEvent(event);
    }
    
    //## statechart_method 
    public int takeEvent(RiJEvent event) {
        return reactive.takeEvent(event);
    }
    
    //## statechart_method 
    public void run() {
        m_thread.run();
    }
    
    //## statechart_method 
    public void start() {
        m_thread.start(this);
    }
    
    // Constructors
    
    //## auto_generated 
    public  Komunikat(RiJThread p_thread) {
        try {
            animInstance().notifyConstructorEntered(animClassKomunikat.getUserClass(),
               new ArgData[] {
               });
        
        m_thread = new RiJThread("Komunikat");
        reactive = new Reactive(m_thread);
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    /**
     * @param komunikat
    */
    //## operation ustaw_komunikat(String) 
    public void ustaw_komunikat(final String komunikat) {
        try {
            animInstance().notifyMethodEntered("ustaw_komunikat",
               new ArgData[] {
                   new ArgData(String.class, "komunikat", AnimInstance.animToString(komunikat))
               });
        
        //#[ operation ustaw_komunikat(String) 
        this.komunikat = komunikat;
        wyswietlaj();
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    //## TriggeredOperation wyswietlaj() 
    public void wyswietlaj() {
        try {
            animInstance().notifyTrgOpEntered("wyswietlaj",
               new ArgData[] {
               });
        
        wyswietlaj_Komunikat_Event triggerEvent = new wyswietlaj_Komunikat_Event();
        reactive.takeTrigger(triggerEvent);
        }
        finally {
            animInstance().notifyTrgOpExit();
        }
        
    }
    
    //## auto_generated 
    public String getKomunikat() {
        return komunikat;
    }
    
    //## auto_generated 
    public void setKomunikat(String p_komunikat) {
        try {
        komunikat = p_komunikat;
        }
        finally {
            animInstance().notifyUpdatedAttr();
        }
    }
    
    //## auto_generated 
    public Automat getAutomat() {
        return automat;
    }
    
    //## auto_generated 
    public void __setAutomat(Automat p_Automat) {
        automat = p_Automat;
        if(p_Automat != null)
            {
                animInstance().notifyRelationAdded("automat", p_Automat);
            }
        else
            {
                animInstance().notifyRelationCleared("automat");
            }
    }
    
    //## auto_generated 
    public void _setAutomat(Automat p_Automat) {
        if(automat != null)
            {
                automat.__setItsKomunikat(null);
            }
        __setAutomat(p_Automat);
    }
    
    //## auto_generated 
    public void setAutomat(Automat p_Automat) {
        if(p_Automat != null)
            {
                p_Automat._setItsKomunikat(this);
            }
        _setAutomat(p_Automat);
    }
    
    //## auto_generated 
    public void _clearAutomat() {
        animInstance().notifyRelationCleared("automat");
        automat = null;
    }
    
    //## auto_generated 
    public boolean startBehavior() {
        boolean done = false;
        done = reactive.startBehavior();
        if(done)
            {
                start();
            }
        return done;
    }
    
    //## ignore 
    public class Reactive extends RiJStateReactive implements AnimatedReactive {
        
        // Default constructor 
        public Reactive() {
            this(RiJMainThread.instance());
        }
        
        
        // Constructors
        
        public  Reactive(RiJThread p_thread) {
            super(p_thread);
            initStatechart();
        }
        
        //## statechart_method 
        public boolean isIn(int state) {
            if(rootState_subState == state)
                {
                    return true;
                }
            return false;
        }
        
        //## statechart_method 
        public boolean isCompleted(int state) {
            return true;
        }
        
        //## statechart_method 
        public void rootState_add(AnimStates animStates) {
            animStates.add("ROOT");
            switch (rootState_subState) {
                case pusty:
                {
                    pusty_add(animStates);
                }
                break;
                case wyswietlanie:
                {
                    wyswietlanie_add(animStates);
                }
                break;
                default:
                    break;
            }
        }
        
        //## statechart_method 
        public void rootState_entDef() {
            {
                rootState_enter();
                rootStateEntDef();
            }
        }
        
        //## statechart_method 
        public int rootState_dispatchEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            switch (rootState_active) {
                case pusty:
                {
                    res = pusty_takeEvent(id);
                }
                break;
                case wyswietlanie:
                {
                    res = wyswietlanie_takeEvent(id);
                }
                break;
                default:
                    break;
            }
            return res;
        }
        
        //## statechart_method 
        public void wyswietlanie_add(AnimStates animStates) {
            animStates.add("ROOT.wyswietlanie");
        }
        
        //## statechart_method 
        public void pusty_add(AnimStates animStates) {
            animStates.add("ROOT.pusty");
        }
        
        //## auto_generated 
        protected void initStatechart() {
            rootState_subState = RiJNonState;
            rootState_active = RiJNonState;
        }
        
        //## statechart_method 
        public int pustyTakewyswietlaj_Komunikat_Event() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            animInstance().notifyTransitionStarted("0");
            pusty_exit();
            wyswietlanie_entDef();
            animInstance().notifyTransitionEnded("0");
            res = RiJStateReactive.TAKE_EVENT_COMPLETE;
            return res;
        }
        
        //## statechart_method 
        public void wyswietlanie_exit() {
            wyswietlanieExit();
            animInstance().notifyStateExited("ROOT.wyswietlanie");
        }
        
        //## statechart_method 
        public void pusty_exit() {
            pustyExit();
            animInstance().notifyStateExited("ROOT.pusty");
        }
        
        //## statechart_method 
        public int wyswietlanieTakeRiJTimeout() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.getTimeoutId() == Komunikat_Timeout_wyswietlanie_id)
                {
                    animInstance().notifyTransitionStarted("1");
                    wyswietlanie_exit();
                    pusty_entDef();
                    animInstance().notifyTransitionEnded("1");
                    res = RiJStateReactive.TAKE_EVENT_COMPLETE;
                }
            return res;
        }
        
        //## statechart_method 
        public int pusty_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.isTypeOf(wyswietlaj_Komunikat_Event.wyswietlaj_Komunikat_Event_id))
                {
                    res = pustyTakewyswietlaj_Komunikat_Event();
                }
            
            return res;
        }
        
        //## statechart_method 
        public int rootState_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            return res;
        }
        
        //## statechart_method 
        public void pusty_enter() {
            animInstance().notifyStateEntered("ROOT.pusty");
            rootState_subState = pusty;
            rootState_active = pusty;
            pustyEnter();
        }
        
        //## statechart_method 
        public void rootState_enter() {
            animInstance().notifyStateEntered("ROOT");
            rootStateEnter();
        }
        
        //## statechart_method 
        public void rootStateEnter() {
        }
        
        //## statechart_method 
        public void wyswietlanie_enter() {
            animInstance().notifyStateEntered("ROOT.wyswietlanie");
            rootState_subState = wyswietlanie;
            rootState_active = wyswietlanie;
            wyswietlanieEnter();
        }
        
        //## statechart_method 
        public void rootStateEntDef() {
            animInstance().notifyTransitionStarted("2");
            pusty_entDef();
            animInstance().notifyTransitionEnded("2");
        }
        
        //## statechart_method 
        public void pustyExit() {
        }
        
        //## statechart_method 
        public void pusty_entDef() {
            pusty_enter();
        }
        
        //## statechart_method 
        public void wyswietlanie_entDef() {
            wyswietlanie_enter();
        }
        
        //## statechart_method 
        public int wyswietlanieTakewyswietlaj_Komunikat_Event() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            animInstance().notifyTransitionStarted("3");
            wyswietlanie_exit();
            wyswietlanie_entDef();
            animInstance().notifyTransitionEnded("3");
            res = RiJStateReactive.TAKE_EVENT_COMPLETE;
            return res;
        }
        
        //## statechart_method 
        public void wyswietlanieExit() {
            itsRiJThread.unschedTimeout(Komunikat_Timeout_wyswietlanie_id, this);
        }
        
        //## statechart_method 
        public void rootStateExit() {
        }
        
        //## statechart_method 
        public void wyswietlanieEnter() {
            itsRiJThread.schedTimeout(5000, Komunikat_Timeout_wyswietlanie_id, this, "ROOT.wyswietlanie");
        }
        
        //## statechart_method 
        public void pustyEnter() {
            //#[ state pusty.(Entry) 
            ustaw_komunikat(" ");
            //#]
        }
        
        //## statechart_method 
        public int wyswietlanie_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.isTypeOf(wyswietlaj_Komunikat_Event.wyswietlaj_Komunikat_Event_id))
                {
                    res = wyswietlanieTakewyswietlaj_Komunikat_Event();
                }
            else if(event.isTypeOf(RiJEvent.TIMEOUT_EVENT_ID))
                {
                    res = wyswietlanieTakeRiJTimeout();
                }
            
            return res;
        }
        
        /**  methods added just for design level debugging instrumentation */
        public boolean startBehavior() {
            try {
              animInstance().notifyBehavioralMethodEntered("startBehavior",
                  new ArgData[] {
                   });
              return super.startBehavior();
            }
            finally {
              animInstance().notifyMethodExit();
            }
        }
        public int takeEvent(RiJEvent event) { 
            try { 
              //animInstance().notifyTakeEvent(new AnimEvent(event));
              animInstance().notifyBehavioralMethodEntered("takeEvent",
                  new ArgData[] { new ArgData(RiJEvent.class, "event", event.toString())
                   });
              return super.takeEvent(event); 
            }
            finally { 
              animInstance().notifyMethodExit();
            }
        }
        /**  see com.ibm.rational.rhapsody.animation.AnimatedReactive interface */
        public AnimInstance animInstance() { 
            return Komunikat.this.animInstance(); 
        }
        
    }
    //#[ ignore
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public AnimClass getAnimClass() { 
        return animClassKomunikat; 
    }
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public Object getFieldValue(java.lang.reflect.Field f, Object userInstance) { 
         Object obj = null;
         try {
             obj = f.get(userInstance);
         } catch(Exception e) {
              java.lang.System.err.println("Exception: getting Field value: " + e);
              e.printStackTrace();
         }
         return obj;
    }
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public AnimInstance animInstance() {
        if (animate == null) 
            animate = new Animate(); 
        return animate; 
    } 
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public void addAttributes(AnimAttributes msg) {
        
        msg.add("komunikat", komunikat);
    }
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public void addRelations(AnimRelations msg) {
        
        msg.add("automat", false, true, automat);
    }
    /** An inner class added as instrumentation for animation */
    public class Animate extends AnimInstance { 
        public  Animate() { 
            super(Komunikat.this); 
        } 
        public void addAttributes(AnimAttributes msg) {
            Komunikat.this.addAttributes(msg);
        }
        public void addRelations(AnimRelations msg) {
            Komunikat.this.addRelations(msg);
        }
        
        public void addStates(AnimStates msg) {
            if ((reactive != null) && (reactive.isTerminated() == false))
              reactive.rootState_add(msg);
        }
        
    } 
    //#]
    
}
//## ignore 
class wyswietlaj_Komunikat_Event extends RiJEvent {
    
    public static final int wyswietlaj_Komunikat_Event_id = 31000;
    
    // Constructors
    
    public  wyswietlaj_Komunikat_Event() {
        lId = wyswietlaj_Komunikat_Event_id;
    }
    
}
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Default/Komunikat.java
*********************************************************************/

