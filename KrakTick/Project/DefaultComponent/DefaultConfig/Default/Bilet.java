/*********************************************************************
	Rhapsody	: 8.0.4
	Login		: Michal
	Component	: DefaultComponent
	Configuration 	: DefaultConfig
	Model Element	: Bilet
//!	Generated Date	: Thu, 21, Nov 2013 
	File Path	: DefaultComponent/DefaultConfig/Default/Bilet.java
*********************************************************************/

package Default;

//## auto_generated
import com.ibm.rational.rhapsody.animation.*;
//## auto_generated
import com.ibm.rational.rhapsody.animcom.animMessages.*;

//----------------------------------------------------------------------------
// Default/Bilet.java                                                                  
//----------------------------------------------------------------------------

//## package Default 


//## class Bilet 
public class Bilet implements Animated {
    
    //#[ ignore
    // Instrumentation attributes (Animation)
    private Animate animate;
    
    public static AnimClass animClassBilet = new AnimClass("Default.Bilet",false);
    public static AnimClass animClassBilet$Rodzaj = new AnimClass("Default.Bilet$Rodzaj",false);
    public static AnimClass animClassBilet$Czas = new AnimClass("Default.Bilet$Czas",false);
    public static AnimClass animClassBilet$Ulga = new AnimClass("Default.Bilet$Ulga",false);
    //#]
    
    protected Bilet.Czas czas;		//## attribute czas 
    
    protected Bilet.Rodzaj rodzaj;		//## attribute rodzaj 
    
    protected Bilet.Ulga ulga;		//## attribute ulga 
    
    
    // Constructors
    
    /**
     * @param czas
     * @param rodzaj
     * @param ulga
    */
    //## operation Bilet(Czas,Rodzaj,Ulga) 
    public  Bilet(final Bilet.Czas czas, final Bilet.Rodzaj rodzaj, final Bilet.Ulga ulga) {
        this.czas = czas;
        this.ulga = ulga;
        this.rodzaj = rodzaj;;
        try {
            animInstance().notifyConstructorEntered(animClassBilet.getUserClass(),
               new ArgData[] {
                   new ArgData(Bilet.Czas.class, "czas", AnimInstance.animToString(czas)),
                   new ArgData(Bilet.Rodzaj.class, "rodzaj", AnimInstance.animToString(rodzaj)),
                   new ArgData(Bilet.Ulga.class, "ulga", AnimInstance.animToString(ulga))
               });
        
        //#[ operation Bilet(Czas,Rodzaj,Ulga) 
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    /**
     * @param czasInt
     * @param rodzajInt
     * @param ulgaInt
    */
    //## operation Bilet(int,int,int) 
    public  Bilet(int czasInt, int rodzajInt, int ulgaInt) {
        try {
            animInstance().notifyConstructorEntered(animClassBilet.getUserClass(),
               new ArgData[] {
                   new ArgData(int.class, "czasInt", AnimInstance.animToString(czasInt)),
                   new ArgData(int.class, "rodzajInt", AnimInstance.animToString(rodzajInt)),
                   new ArgData(int.class, "ulgaInt", AnimInstance.animToString(ulgaInt))
               });
        
        //#[ operation Bilet(int,int,int) 
        switch(czasInt) {
        	case 20:
        		this.czas = Bilet.Czas.MIN_20;    
        		break;
        	case 40:
        		this.czas = Bilet.Czas.MIN_40;  
        		break;
        	case 60:
        		this.czas = Bilet.Czas.MIN_60;	
        		break;	
        	default:
        		throw new java.lang.NullPointerException();		
        }      
        
        switch(rodzajInt) {
        	case 1:
        		this.rodzaj = Bilet.Rodzaj.MIEJSKI;    
        		break;
        	case 2:
        		this.rodzaj = Bilet.Rodzaj.AGLOMERACYJNY; 
        		break;
        }                                        
        
        switch(ulgaInt) {
        	case 1:
        		this.ulga = Bilet.Ulga.ULGOWY;        
        		break;
        	case 2:
        		this.ulga = Bilet.Ulga.NORMALNY;     
        		break;
        }
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    //## auto_generated 
    public  Bilet() {
        try {
            animInstance().notifyConstructorEntered(animClassBilet.getUserClass(),
               new ArgData[] {
               });
        
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    //## operation cena() 
    public int cena() {
        try {
            animInstance().notifyMethodEntered("cena",
               new ArgData[] {
               });
        
        //#[ operation cena() 
        return (czas.value + rodzaj.value) / (ulga.value);
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    /**
     * @param obj
    */
    //## operation equals(Object) 
    public boolean equals(final Object obj) {
        try {
            animInstance().notifyMethodEntered("equals",
               new ArgData[] {
                   new ArgData(Object.class, "obj", AnimInstance.animToString(obj))
               });
        
        //#[ operation equals(Object) 
        Bilet b = (Bilet) obj;
        
        System.out.println(this.string());
        System.out.println(b.string());
        System.out.println();
        
        if(this.czas.equals(b.czas)) {
        	if(this.rodzaj.equals(b.rodzaj)) {
        		if(this.ulga.equals(b.ulga)) {
        			return true;
        		}
        	}
        }
        return false;
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    //## operation string() 
    public String string() {
        try {
            animInstance().notifyMethodEntered("string",
               new ArgData[] {
               });
        
        //#[ operation string() 
        return 
        	(ulga == Ulga.ULGOWY ? "Ulgowy" : "Normalny") + " " +
        	(rodzaj == Rodzaj.MIEJSKI ? "miejski" : "aglomeracyjny") + " " +
        		(280 == czas.value ? "20" : (380 == czas.value ? "40" : "60")) + " min";
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    //## auto_generated 
    public Bilet.Czas getCzas() {
        return czas;
    }
    
    //## auto_generated 
    public void setCzas(Bilet.Czas p_czas) {
        czas = p_czas;
    }
    
    //## auto_generated 
    public Bilet.Rodzaj getRodzaj() {
        return rodzaj;
    }
    
    //## auto_generated 
    public void setRodzaj(Bilet.Rodzaj p_rodzaj) {
        rodzaj = p_rodzaj;
    }
    
    //## auto_generated 
    public Bilet.Ulga getUlga() {
        return ulga;
    }
    
    //## auto_generated 
    public void setUlga(Bilet.Ulga p_ulga) {
        ulga = p_ulga;
    }
    
    //## class Bilet::Rodzaj 
    public enum Rodzaj {
        AGLOMERACYJNY(100),
        MIEJSKI(0);
        
        
        protected int value;		//## attribute value 
        
        
        // Constructors
        
        /**
         * @param doplata
        */
        //## operation Rodzaj(int) 
         Rodzaj(int doplata) {
            this.value = doplata;;
            //#[ operation Rodzaj(int) 
            //#]
        }
        //## auto_generated 
         Rodzaj() {
        }
        
        //## auto_generated 
        public int getValue() {
            return value;
        }
        
        //## auto_generated 
        public void setValue(int p_value) {
            value = p_value;
        }
        
    }
    //## class Bilet::Czas 
    public enum Czas {
        MIN_20(280),
        MIN_40(380),
        MIN_60(500);
        
        
        protected int value;		//## attribute value 
        
        
        // Constructors
        
        /**
         * @param price
        */
        //## operation Czas(int) 
         Czas(int price) {
            this.value = price;;
            //#[ operation Czas(int) 
            //#]
        }
        //## auto_generated 
         Czas() {
        }
        
        //## auto_generated 
        public int getValue() {
            return value;
        }
        
        //## auto_generated 
        public void setValue(int p_value) {
            value = p_value;
        }
        
    }
    //## class Bilet::Ulga 
    public enum Ulga {
        ULGOWY(2),
        NORMALNY(1);
        
        
        protected int value;		//## attribute value 
        
        
        // Constructors
        
        /**
         * @param dzielnik
        */
        //## operation Ulga(int) 
         Ulga(int dzielnik) {
            this.value = dzielnik;;
            //#[ operation Ulga(int) 
            //#]
        }
        //## auto_generated 
         Ulga() {
        }
        
        //## auto_generated 
        public int getValue() {
            return value;
        }
        
        //## auto_generated 
        public void setValue(int p_value) {
            value = p_value;
        }
        
    }
    //#[ ignore
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public AnimClass getAnimClass() { 
        return animClassBilet; 
    }
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public Object getFieldValue(java.lang.reflect.Field f, Object userInstance) { 
         Object obj = null;
         try {
             obj = f.get(userInstance);
         } catch(Exception e) {
              java.lang.System.err.println("Exception: getting Field value: " + e);
              e.printStackTrace();
         }
         return obj;
    }
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public AnimInstance animInstance() {
        if (animate == null) 
            animate = new Animate(); 
        return animate; 
    } 
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public void addAttributes(AnimAttributes msg) {
        
        msg.add("czas", czas);
        msg.add("rodzaj", rodzaj);
        msg.add("ulga", ulga);
    }
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public void addRelations(AnimRelations msg) {
        
    }
    /** An inner class added as instrumentation for animation */
    public class Animate extends AnimInstance { 
        public  Animate() { 
            super(Bilet.this); 
        } 
        public void addAttributes(AnimAttributes msg) {
            Bilet.this.addAttributes(msg);
        }
        public void addRelations(AnimRelations msg) {
            Bilet.this.addRelations(msg);
        }
        
    } 
    //#]
    
}
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Default/Bilet.java
*********************************************************************/

