/*********************************************************************
	Rhapsody	: 8.0.4
	Login		: Michal
	Component	: DefaultComponent
	Configuration 	: DefaultConfig
	Model Element	: zmniejsz_ilosc
//!	Generated Date	: Sun, 17, Nov 2013 
	File Path	: DefaultComponent/DefaultConfig/Default/zmniejsz_ilosc.java
*********************************************************************/

package Default;

//## auto_generated
import com.ibm.rational.rhapsody.animation.*;
//## auto_generated
import com.ibm.rational.rhapsody.oxf.RiJEvent;
//## auto_generated
import com.ibm.rational.rhapsody.animcom.animMessages.*;

//----------------------------------------------------------------------------
// Default/zmniejsz_ilosc.java                                                                  
//----------------------------------------------------------------------------

//## package Default 


//## event zmniejsz_ilosc() 
public class zmniejsz_ilosc extends RiJEvent implements AnimatedEvent {
    
    public static final int zmniejsz_ilosc_Default_id = 18624;		//## ignore 
    
    
    // Constructors
    
    public  zmniejsz_ilosc() {
        lId = zmniejsz_ilosc_Default_id;
    }
    
    public boolean isTypeOf(long id) {
        return (zmniejsz_ilosc_Default_id==id);
    }
    
    //#[ ignore
    /** the animated event proxy */
    public static AnimEventClass animClass = new AnimEventClass("Default.zmniejsz_ilosc");
    /**  see com.ibm.rational.rhapsody.animation.AnimatedEvent interface */
    public Object getFieldValue(java.lang.reflect.Field f, Object userInstance) { 
         Object obj = null;
         try {
             obj = f.get(userInstance);
         } catch(Exception e) {
              java.lang.System.err.println("Exception: getting Field value: " + e);
              e.printStackTrace();
         }
         return obj;
    }
    /**  see com.ibm.rational.rhapsody.animation.AnimatedEvent interface */
    public void addAttributes(AnimAttributes msg) {      
    }
    public String toString() {
          String s="zmniejsz_ilosc(";      
          s += ")";
          return s;
    }
    //#]
    
}
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Default/zmniejsz_ilosc.java
*********************************************************************/

