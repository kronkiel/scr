/*********************************************************************
	Rhapsody	: 8.0.4
	Login		: Michal
	Component	: DefaultComponent
	Configuration 	: DefaultConfig
	Model Element	: Koszyk
//!	Generated Date	: Thu, 21, Nov 2013 
	File Path	: DefaultComponent/DefaultConfig/Default/Koszyk.java
*********************************************************************/

package Default;

//## auto_generated
import com.ibm.rational.rhapsody.oxf.*;
//## auto_generated
import com.ibm.rational.rhapsody.animation.*;
//## auto_generated
import com.ibm.rational.rhapsody.oxf.states.*;
//## auto_generated
import com.ibm.rational.rhapsody.animcom.animMessages.*;

//----------------------------------------------------------------------------
// Default/Koszyk.java                                                                  
//----------------------------------------------------------------------------

//## package Default 


//## class Koszyk 
public class Koszyk implements RiJStateConcept, Animated {
    
    //#[ ignore
    // Instrumentation attributes (Animation)
    private Animate animate;
    
    public static AnimClass animClassKoszyk = new AnimClass("Default.Koszyk",false);
    //#]
    
    public Reactive reactive;		//## ignore 
    
    protected java.util.List<Bilet> bilety = new java.util.LinkedList<Bilet>();		//## attribute bilety 
    
    protected int wybrany_czas = 60;		//## attribute wybrany_czas 
    
    protected int wybrany_ilosc = 1;		//## attribute wybrany_ilosc 
    
    protected int wybrany_rodzaj = 2;		//## attribute wybrany_rodzaj 
    
    protected int wybrany_ulga = 2;		//## attribute wybrany_ulga 
    
    protected Automat automat;		//## link automat 
    
    //#[ ignore 
    public static final int RiJNonState=0;
    public static final int idle=1;
    public static final int czysc_koszyk=2;
    public static final int akcja_gui=3;
    //#]
    protected int rootState_subState;		//## ignore 
    
    protected int rootState_active;		//## ignore 
    
    public static final int Koszyk_Timeout_idle_id = 1;		//## ignore 
    
    
    //## statechart_method 
    public RiJThread getThread() {
        return reactive.getThread();
    }
    
    //## statechart_method 
    public void schedTimeout(long delay, long tmID, RiJStateReactive reactive) {
        getThread().schedTimeout(delay, tmID, reactive);
    }
    
    //## statechart_method 
    public void unschedTimeout(long tmID, RiJStateReactive reactive) {
        getThread().unschedTimeout(tmID, reactive);
    }
    
    //## statechart_method 
    public boolean isIn(int state) {
        return reactive.isIn(state);
    }
    
    //## statechart_method 
    public boolean isCompleted(int state) {
        return reactive.isCompleted(state);
    }
    
    //## statechart_method 
    public RiJEventConsumer getEventConsumer() {
        return (RiJEventConsumer)reactive;
    }
    
    //## statechart_method 
    public void gen(RiJEvent event) {
        reactive._gen(event);
    }
    
    //## statechart_method 
    public void queueEvent(RiJEvent event) {
        reactive.queueEvent(event);
    }
    
    //## statechart_method 
    public int takeEvent(RiJEvent event) {
        return reactive.takeEvent(event);
    }
    
    // Constructors
    
    //## auto_generated 
    public  Koszyk(RiJThread p_thread) {
        try {
            animInstance().notifyConstructorEntered(animClassKoszyk.getUserClass(),
               new ArgData[] {
               });
        
        reactive = new Reactive(p_thread);
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    //## operation dodaj_bilet() 
    public void dodaj_bilet() {
        try {
            animInstance().notifyMethodEntered("dodaj_bilet",
               new ArgData[] {
               });
        
        //#[ operation dodaj_bilet() 
        try {  
        	int dostepnaIlosc = automat.itsBileter.ileJest(new Bilet(wybrany_czas, wybrany_rodzaj, wybrany_ulga));   
        	if(dostepnaIlosc >= wybrany_ilosc) {    
        		int cena =  new Bilet(wybrany_czas, wybrany_rodzaj, wybrany_ulga).cena() * wybrany_ilosc;
        		if(koszt() + cena > 5000) {
        			automat.ustaw_komunikat("Przekroczono sume 50zl"); 
        		} else {
        			for(int i = 0; i < wybrany_ilosc; ++i){   
        				Bilet b = new Bilet(wybrany_czas, wybrany_rodzaj, wybrany_ulga);
        				automat.itsBileter.zabierz(b);
        				bilety.add(b);   
        			}
        			odswiez();          
        	}	}
        	else {
        		automat.ustaw_komunikat("Brak wystarczajacej ilosci biletow!"); 
        		this.wybrany_ilosc = dostepnaIlosc;
        	}
        } catch(Exception e) {
        	System.out.println("Zle parametry biletu!");
        }
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    //## operation koszt() 
    public int koszt() {
        try {
            animInstance().notifyMethodEntered("koszt",
               new ArgData[] {
               });
        
        //#[ operation koszt() 
        int suma = 0;
        for(Bilet b : bilety) {
        	suma += b.cena();
        }                 
        
        java.util.List<Bilet> tmp = new java.util.LinkedList<Bilet>(bilety);
        int rabat = 0;
        while(!tmp.isEmpty()) {
        	Bilet bil = tmp.get(0);
        	int occurrences = java.util.Collections.frequency(tmp, bil);
        	int ileRabatow = occurrences / 5;
        	rabat += 5 * ileRabatow * (  0.1 * bil.cena() );
        	while(tmp.remove(bil));
        }         
        
        return suma - rabat;
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    //## operation odswiez() 
    protected void odswiez() {
        try {
            animInstance().notifyMethodEntered("odswiez",
               new ArgData[] {
               });
        
        //#[ operation odswiez() 
        this.wybrany_czas = 60;
        this.wybrany_ulga = 2;
        this.wybrany_rodzaj = 2;
        if(automat.itsBileter.ileJest(new Bilet(wybrany_czas, wybrany_ulga, wybrany_rodzaj)) > 0)
        	this.wybrany_ilosc = 1;                                                               
        else
        	this.wybrany_ilosc = 0;
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    //## operation wyczysc() 
    public void wyczysc() {
        try {
            animInstance().notifyMethodEntered("wyczysc",
               new ArgData[] {
               });
        
        //#[ operation wyczysc() 
        for(Bilet b : bilety) {
          automat.itsBileter.zwroc(b);
        }
        bilety.clear();
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    //## auto_generated 
    public java.util.List<Bilet> getBilety() {
        return bilety;
    }
    
    //## auto_generated 
    public void setBilety(java.util.List<Bilet> p_bilety) {
        bilety = p_bilety;
    }
    
    //## auto_generated 
    public int getWybrany_czas() {
        return wybrany_czas;
    }
    
    //## auto_generated 
    public void setWybrany_czas(int p_wybrany_czas) {
        try {
        wybrany_czas = p_wybrany_czas;
        }
        finally {
            animInstance().notifyUpdatedAttr();
        }
    }
    
    //## auto_generated 
    public int getWybrany_ilosc() {
        return wybrany_ilosc;
    }
    
    //## auto_generated 
    public void setWybrany_ilosc(int p_wybrany_ilosc) {
        try {
        wybrany_ilosc = p_wybrany_ilosc;
        }
        finally {
            animInstance().notifyUpdatedAttr();
        }
    }
    
    //## auto_generated 
    public int getWybrany_rodzaj() {
        return wybrany_rodzaj;
    }
    
    //## auto_generated 
    public void setWybrany_rodzaj(int p_wybrany_rodzaj) {
        try {
        wybrany_rodzaj = p_wybrany_rodzaj;
        }
        finally {
            animInstance().notifyUpdatedAttr();
        }
    }
    
    //## auto_generated 
    public int getWybrany_ulga() {
        return wybrany_ulga;
    }
    
    //## auto_generated 
    public void setWybrany_ulga(int p_wybrany_ulga) {
        try {
        wybrany_ulga = p_wybrany_ulga;
        }
        finally {
            animInstance().notifyUpdatedAttr();
        }
    }
    
    //## auto_generated 
    public Automat getAutomat() {
        return automat;
    }
    
    //## auto_generated 
    public void __setAutomat(Automat p_Automat) {
        automat = p_Automat;
        if(p_Automat != null)
            {
                animInstance().notifyRelationAdded("automat", p_Automat);
            }
        else
            {
                animInstance().notifyRelationCleared("automat");
            }
    }
    
    //## auto_generated 
    public void _setAutomat(Automat p_Automat) {
        if(automat != null)
            {
                automat.__setItsKoszyk(null);
            }
        __setAutomat(p_Automat);
    }
    
    //## auto_generated 
    public void setAutomat(Automat p_Automat) {
        if(p_Automat != null)
            {
                p_Automat._setItsKoszyk(this);
            }
        _setAutomat(p_Automat);
    }
    
    //## auto_generated 
    public void _clearAutomat() {
        animInstance().notifyRelationCleared("automat");
        automat = null;
    }
    
    //## auto_generated 
    public boolean startBehavior() {
        boolean done = false;
        done = reactive.startBehavior();
        return done;
    }
    
    //## ignore 
    public class Reactive extends RiJStateReactive implements AnimatedReactive {
        
        // Default constructor 
        public Reactive() {
            this(RiJMainThread.instance());
        }
        
        
        // Constructors
        
        public  Reactive(RiJThread p_thread) {
            super(p_thread);
            initStatechart();
        }
        
        //## statechart_method 
        public boolean isIn(int state) {
            if(rootState_subState == state)
                {
                    return true;
                }
            return false;
        }
        
        //## statechart_method 
        public boolean isCompleted(int state) {
            return true;
        }
        
        //## statechart_method 
        public void rootState_add(AnimStates animStates) {
            animStates.add("ROOT");
            switch (rootState_subState) {
                case idle:
                {
                    idle_add(animStates);
                }
                break;
                case akcja_gui:
                {
                    akcja_gui_add(animStates);
                }
                break;
                case czysc_koszyk:
                {
                    czysc_koszyk_add(animStates);
                }
                break;
                default:
                    break;
            }
        }
        
        //## statechart_method 
        public void rootState_entDef() {
            {
                rootState_enter();
                rootStateEntDef();
            }
        }
        
        //## statechart_method 
        public int rootState_dispatchEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            switch (rootState_active) {
                case idle:
                {
                    res = idle_takeEvent(id);
                }
                break;
                case akcja_gui:
                {
                    res = akcja_gui_takeEvent(id);
                }
                break;
                case czysc_koszyk:
                {
                    res = czysc_koszyk_takeEvent(id);
                }
                break;
                default:
                    break;
            }
            return res;
        }
        
        //## statechart_method 
        public void idle_add(AnimStates animStates) {
            animStates.add("ROOT.idle");
        }
        
        //## statechart_method 
        public void czysc_koszyk_add(AnimStates animStates) {
            animStates.add("ROOT.czysc_koszyk");
        }
        
        //## statechart_method 
        public void akcja_gui_add(AnimStates animStates) {
            animStates.add("ROOT.akcja_gui");
        }
        
        //## auto_generated 
        protected void initStatechart() {
            rootState_subState = RiJNonState;
            rootState_active = RiJNonState;
        }
        
        //## statechart_method 
        public void czysc_koszyk_entDef() {
            czysc_koszyk_enter();
        }
        
        //## statechart_method 
        public void idle_entDef() {
            idle_enter();
        }
        
        //## statechart_method 
        public void akcja_guiExit() {
        }
        
        //## statechart_method 
        public int czysc_koszyk_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.isTypeOf(RiJEvent.NULL_EVENT_ID))
                {
                    res = czysc_koszykTakeNull();
                }
            
            return res;
        }
        
        //## statechart_method 
        public int idleTakeRiJTimeout() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.getTimeoutId() == Koszyk_Timeout_idle_id)
                {
                    animInstance().notifyTransitionStarted("6");
                    idle_exit();
                    czysc_koszyk_entDef();
                    animInstance().notifyTransitionEnded("6");
                    res = RiJStateReactive.TAKE_EVENT_COMPLETE;
                }
            return res;
        }
        
        //## statechart_method 
        public void idleEnter() {
            //#[ state idle.(Entry) 
            automat.odswiez();
            
            //#]
            itsRiJThread.schedTimeout(20000, Koszyk_Timeout_idle_id, this, "ROOT.idle");
        }
        
        //## statechart_method 
        public void czysc_koszyk_enter() {
            animInstance().notifyStateEntered("ROOT.czysc_koszyk");
            pushNullConfig();
            rootState_subState = czysc_koszyk;
            rootState_active = czysc_koszyk;
            czysc_koszykEnter();
        }
        
        //## statechart_method 
        public void idle_exit() {
            idleExit();
            animInstance().notifyStateExited("ROOT.idle");
        }
        
        //## statechart_method 
        public int akcja_guiTakeNull() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            animInstance().notifyTransitionStarted("4");
            akcja_gui_exit();
            idle_entDef();
            animInstance().notifyTransitionEnded("4");
            res = RiJStateReactive.TAKE_EVENT_COMPLETE;
            return res;
        }
        
        //## statechart_method 
        public void akcja_gui_entDef() {
            akcja_gui_enter();
        }
        
        //## statechart_method 
        public int rootState_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            return res;
        }
        
        //## statechart_method 
        public int idleTakegui_dodaj_bilet() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            animInstance().notifyTransitionStarted("3");
            idle_exit();
            //#[ transition 3 
            dodaj_bilet();
            //#]
            akcja_gui_entDef();
            animInstance().notifyTransitionEnded("3");
            res = RiJStateReactive.TAKE_EVENT_COMPLETE;
            return res;
        }
        
        //## statechart_method 
        public void czysc_koszykEnter() {
            //#[ state czysc_koszyk.(Entry) 
            automat.anuluj();
            //#]
        }
        
        //## statechart_method 
        public void rootState_enter() {
            animInstance().notifyStateEntered("ROOT");
            rootStateEnter();
        }
        
        //## statechart_method 
        public void rootStateEnter() {
        }
        
        //## statechart_method 
        public void akcja_guiEnter() {
        }
        
        //## statechart_method 
        public void czysc_koszyk_exit() {
            popNullConfig();
            czysc_koszykExit();
            animInstance().notifyStateExited("ROOT.czysc_koszyk");
        }
        
        //## statechart_method 
        public void idle_enter() {
            animInstance().notifyStateEntered("ROOT.idle");
            rootState_subState = idle;
            rootState_active = idle;
            idleEnter();
        }
        
        //## statechart_method 
        public void rootStateEntDef() {
            animInstance().notifyTransitionStarted("0");
            czysc_koszyk_entDef();
            animInstance().notifyTransitionEnded("0");
        }
        
        //## statechart_method 
        public void akcja_gui_exit() {
            popNullConfig();
            akcja_guiExit();
            animInstance().notifyStateExited("ROOT.akcja_gui");
        }
        
        //## statechart_method 
        public void akcja_gui_enter() {
            animInstance().notifyStateEntered("ROOT.akcja_gui");
            pushNullConfig();
            rootState_subState = akcja_gui;
            rootState_active = akcja_gui;
            akcja_guiEnter();
        }
        
        //## statechart_method 
        public int czysc_koszykTakeNull() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            animInstance().notifyTransitionStarted("5");
            czysc_koszyk_exit();
            idle_entDef();
            animInstance().notifyTransitionEnded("5");
            res = RiJStateReactive.TAKE_EVENT_COMPLETE;
            return res;
        }
        
        //## statechart_method 
        public int idleTakegui_zwieksz_ilosc_biletow() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            animInstance().notifyTransitionStarted("2");
            idle_exit();
            //#[ transition 2 
            if(wybrany_ilosc < 9) ++wybrany_ilosc;
            //#]
            akcja_gui_entDef();
            animInstance().notifyTransitionEnded("2");
            res = RiJStateReactive.TAKE_EVENT_COMPLETE;
            return res;
        }
        
        //## statechart_method 
        public int idleTakegui_zmniejsz_ilosc_biletow() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            animInstance().notifyTransitionStarted("1");
            idle_exit();
            //#[ transition 1 
            if(wybrany_ilosc > 1) --wybrany_ilosc;
            //#]
            akcja_gui_entDef();
            animInstance().notifyTransitionEnded("1");
            res = RiJStateReactive.TAKE_EVENT_COMPLETE;
            return res;
        }
        
        //## statechart_method 
        public int idle_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.isTypeOf(gui_zmniejsz_ilosc_biletow.gui_zmniejsz_ilosc_biletow_Default_id))
                {
                    res = idleTakegui_zmniejsz_ilosc_biletow();
                }
            else if(event.isTypeOf(RiJEvent.TIMEOUT_EVENT_ID))
                {
                    res = idleTakeRiJTimeout();
                }
            else if(event.isTypeOf(gui_zwieksz_ilosc_biletow.gui_zwieksz_ilosc_biletow_Default_id))
                {
                    res = idleTakegui_zwieksz_ilosc_biletow();
                }
            else if(event.isTypeOf(gui_dodaj_bilet.gui_dodaj_bilet_Default_id))
                {
                    res = idleTakegui_dodaj_bilet();
                }
            
            return res;
        }
        
        //## statechart_method 
        public int akcja_gui_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.isTypeOf(RiJEvent.NULL_EVENT_ID))
                {
                    res = akcja_guiTakeNull();
                }
            
            return res;
        }
        
        //## statechart_method 
        public void rootStateExit() {
        }
        
        //## statechart_method 
        public void czysc_koszykExit() {
        }
        
        //## statechart_method 
        public void idleExit() {
            itsRiJThread.unschedTimeout(Koszyk_Timeout_idle_id, this);
        }
        
        /**  methods added just for design level debugging instrumentation */
        public boolean startBehavior() {
            try {
              animInstance().notifyBehavioralMethodEntered("startBehavior",
                  new ArgData[] {
                   });
              return super.startBehavior();
            }
            finally {
              animInstance().notifyMethodExit();
            }
        }
        public int takeEvent(RiJEvent event) { 
            try { 
              //animInstance().notifyTakeEvent(new AnimEvent(event));
              animInstance().notifyBehavioralMethodEntered("takeEvent",
                  new ArgData[] { new ArgData(RiJEvent.class, "event", event.toString())
                   });
              return super.takeEvent(event); 
            }
            finally { 
              animInstance().notifyMethodExit();
            }
        }
        /**  see com.ibm.rational.rhapsody.animation.AnimatedReactive interface */
        public AnimInstance animInstance() { 
            return Koszyk.this.animInstance(); 
        }
        
    }
    //#[ ignore
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public AnimClass getAnimClass() { 
        return animClassKoszyk; 
    }
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public Object getFieldValue(java.lang.reflect.Field f, Object userInstance) { 
         Object obj = null;
         try {
             obj = f.get(userInstance);
         } catch(Exception e) {
              java.lang.System.err.println("Exception: getting Field value: " + e);
              e.printStackTrace();
         }
         return obj;
    }
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public AnimInstance animInstance() {
        if (animate == null) 
            animate = new Animate(); 
        return animate; 
    } 
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public void addAttributes(AnimAttributes msg) {
        
        msg.add("bilety", bilety);
        msg.add("wybrany_czas", wybrany_czas);
        msg.add("wybrany_rodzaj", wybrany_rodzaj);
        msg.add("wybrany_ulga", wybrany_ulga);
        msg.add("wybrany_ilosc", wybrany_ilosc);
    }
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public void addRelations(AnimRelations msg) {
        
        msg.add("automat", false, true, automat);
    }
    /** An inner class added as instrumentation for animation */
    public class Animate extends AnimInstance { 
        public  Animate() { 
            super(Koszyk.this); 
        } 
        public void addAttributes(AnimAttributes msg) {
            Koszyk.this.addAttributes(msg);
        }
        public void addRelations(AnimRelations msg) {
            Koszyk.this.addRelations(msg);
        }
        
        public void addStates(AnimStates msg) {
            if ((reactive != null) && (reactive.isTerminated() == false))
              reactive.rootState_add(msg);
        }
        
    } 
    //#]
    
}
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Default/Koszyk.java
*********************************************************************/

