/*********************************************************************
	Rhapsody	: 8.0.4
	Login		: Kronkiel
	Component	: DefaultComponent
	Configuration 	: DefaultConfig
	Model Element	: wplac1zl
//!	Generated Date	: Sun, 17, Nov 2013 
	File Path	: DefaultComponent/DefaultConfig/Default/wplac1zl.java
*********************************************************************/

package Default;

//## auto_generated
import com.ibm.rational.rhapsody.animation.*;
//## auto_generated
import com.ibm.rational.rhapsody.oxf.RiJEvent;
//## auto_generated
import com.ibm.rational.rhapsody.animcom.animMessages.*;

//----------------------------------------------------------------------------
// Default/wplac1zl.java                                                                  
//----------------------------------------------------------------------------

//## package Default 


//## event wplac1zl() 
public class wplac1zl extends RiJEvent implements AnimatedEvent {
    
    public static final int wplac1zl_Default_id = 18625;		//## ignore 
    
    
    // Constructors
    
    public  wplac1zl() {
        lId = wplac1zl_Default_id;
    }
    
    public boolean isTypeOf(long id) {
        return (wplac1zl_Default_id==id);
    }
    
    //#[ ignore
    /** the animated event proxy */
    public static AnimEventClass animClass = new AnimEventClass("Default.wplac1zl");
    /**  see com.ibm.rational.rhapsody.animation.AnimatedEvent interface */
    public Object getFieldValue(java.lang.reflect.Field f, Object userInstance) { 
         Object obj = null;
         try {
             obj = f.get(userInstance);
         } catch(Exception e) {
              java.lang.System.err.println("Exception: getting Field value: " + e);
              e.printStackTrace();
         }
         return obj;
    }
    /**  see com.ibm.rational.rhapsody.animation.AnimatedEvent interface */
    public void addAttributes(AnimAttributes msg) {      
    }
    public String toString() {
          String s="wplac1zl(";      
          s += ")";
          return s;
    }
    //#]
    
}
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Default/wplac1zl.java
*********************************************************************/

