echo off

set RHAP_JARS_DIR=C:/Users/Michal/IBM/Rational/Rhapsody/8.0.4x64/Share\LangJava\lib
set FRAMEWORK_NONE_JARS=C:/Users/Michal/IBM/Rational/Rhapsody/8.0.4x64/Share\LangJava\lib\oxf.jar;C:/Users/Michal/IBM/Rational/Rhapsody/8.0.4x64/Share\LangJava\lib\anim.jar;C:/Users/Michal/IBM/Rational/Rhapsody/8.0.4x64/Share\LangJava\lib\animcom.jar;C:/Users/Michal/IBM/Rational/Rhapsody/8.0.4x64/Share\LangJava\lib\oxfInstMock.jar;
set FRAMEWORK_ANIM_JARS=C:/Users/Michal/IBM/Rational/Rhapsody/8.0.4x64/Share\LangJava\lib\oxf.jar;C:/Users/Michal/IBM/Rational/Rhapsody/8.0.4x64/Share\LangJava\lib\anim.jar;C:/Users/Michal/IBM/Rational/Rhapsody/8.0.4x64/Share\LangJava\lib\animcom.jar;C:/Users/Michal/IBM/Rational/Rhapsody/8.0.4x64/Share\LangJava\lib\oxfInst.jar;
set SOURCEPATH=%SOURCEPATH%
set CLASSPATH=%CLASSPATH%;.
set PATH=%RHAP_JARS_DIR%;%PATH%;
set INSTRUMENTATION=Animation   

set BUILDSET=Debug

if %INSTRUMENTATION%==Animation goto anim

:noanim
set CLASSPATH=%CLASSPATH%;%FRAMEWORK_NONE_JARS%
goto setEnv_end

:anim
set CLASSPATH=%CLASSPATH%;%FRAMEWORK_ANIM_JARS%

:setEnv_end

if "%1" == "" goto compile
if "%1" == "build" goto compile
if "%1" == "clean" goto clean
if "%1" == "rebuild" goto clean
if "%1" == "run" goto run

:clean
echo cleaning class files
if exist Default\gui_wplac1zl.class del Default\gui_wplac1zl.class
if exist Default\gui_zwieksz_ilosc_biletow.class del Default\gui_zwieksz_ilosc_biletow.class
if exist Default\gui_uzupelnij_monety.class del Default\gui_uzupelnij_monety.class
if exist Default\Monety.class del Default\Monety.class
if exist Default\gui_wplac50gr.class del Default\gui_wplac50gr.class
if exist MainDefaultComponent.class del MainDefaultComponent.class
if exist Default\gui_zmniejsz_ilosc_biletow.class del Default\gui_zmniejsz_ilosc_biletow.class
if exist Default\gui_anuluj.class del Default\gui_anuluj.class
if exist Default\Komunikat.class del Default\Komunikat.class
if exist Default\gui_wplac20gr.class del Default\gui_wplac20gr.class
if exist Default\Zarzadca.class del Default\Zarzadca.class
if exist Default\gui_oddaj_wplacone.class del Default\gui_oddaj_wplacone.class
if exist Default\Bilet.class del Default\Bilet.class
if exist Default\gui_zabierz_bilety.class del Default\gui_zabierz_bilety.class
if exist Default\Automat.class del Default\Automat.class
if exist Default\gui_wplac10gr.class del Default\gui_wplac10gr.class
if exist Default\Koszyk.class del Default\Koszyk.class
if exist Default\AktualnaCena.class del Default\AktualnaCena.class
if exist Default\gui_wplac5zl.class del Default\gui_wplac5zl.class
if exist Default\gui_zabierz_reszte.class del Default\gui_zabierz_reszte.class
if exist Default\Klient.class del Default\Klient.class
if exist Default\Moneta.class del Default\Moneta.class
if exist Default\gui_wplac2zl.class del Default\gui_wplac2zl.class
if exist Default\gui_dodaj_bilet.class del Default\gui_dodaj_bilet.class
if exist Default\Bileter.class del Default\Bileter.class
if exist Default\gui_uzupelnij_bilety.class del Default\gui_uzupelnij_bilety.class
if exist Default\Default_pkgClass.class del Default\Default_pkgClass.class
if exist Default\Kasa.class del Default\Kasa.class

if "%1" == "clean" goto end

:compile   
if %BUILDSET%==Debug goto compile_debug
echo compiling JAVA source files
javac  @files.lst
goto end

:compile_debug
echo compiling JAVA source files
javac -g  @files.lst
goto end

:run

java %2

:end


