/*********************************************************************
	Rhapsody	: 8.0.4
	Login		: Michal
	Component	: DefaultComponent
	Configuration 	: DefaultConfig
	Model Element	: DefaultConfig
//!	Generated Date	: Thu, 21, Nov 2013 
	File Path	: DefaultComponent/DefaultConfig/MainDefaultComponent.java
*********************************************************************/


//## auto_generated
import Default.*;
//## auto_generated
import com.ibm.rational.rhapsody.oxf.*;
//## auto_generated
import com.ibm.rational.rhapsody.animcom.*;

//----------------------------------------------------------------------------
// MainDefaultComponent.java                                                                  
//----------------------------------------------------------------------------


//## ignore 
public class MainDefaultComponent {
    
    //#[ ignore
    // link with events in order to register them in the animation browser
    static {
      // Setting Animation Default Port 
      AnimTcpIpConnection.setDefaultPort(6423);
      // Registering Events 
      try {
        
            Class.forName("Default.gui_anuluj");
            Class.forName("Default.gui_dodaj_bilet");
            Class.forName("Default.gui_oddaj_wplacone");
            Class.forName("Default.gui_uzupelnij_bilety");
            Class.forName("Default.gui_uzupelnij_monety");
            Class.forName("Default.gui_wplac10gr");
            Class.forName("Default.gui_wplac1zl");
            Class.forName("Default.gui_wplac20gr");
            Class.forName("Default.gui_wplac2zl");
            Class.forName("Default.gui_wplac50gr");
            Class.forName("Default.gui_wplac5zl");
            Class.forName("Default.gui_zabierz_bilety");
            Class.forName("Default.gui_zabierz_reszte");
            Class.forName("Default.gui_zmniejsz_ilosc_biletow");
            Class.forName("Default.gui_zwieksz_ilosc_biletow");
    
        // Registering Static Classes 
        
      }
      catch(Exception e) { 
         java.lang.System.err.println(e.toString());
         e.printStackTrace(java.lang.System.err);
      }
    }
    //#]
    
    protected static Automat p_Automat = null;
    
    //## configuration DefaultComponent::DefaultConfig 
    public static void main(String[] args) {
        RiJOXF.Init(null, 0, 0, true, args);
        MainDefaultComponent initializer_DefaultComponent = new MainDefaultComponent();
        p_Automat = new Automat(RiJMainThread.instance());
        p_Automat.startBehavior();
        //#[ configuration DefaultComponent::DefaultConfig 
        //#]
        RiJOXF.Start();
        p_Automat=null;
    }
    
}
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/MainDefaultComponent.java
*********************************************************************/

